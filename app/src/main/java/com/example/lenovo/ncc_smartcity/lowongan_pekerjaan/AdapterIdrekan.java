package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.ListIdrekan;


import java.util.List;

/**
 * Created by arimahardika on 19/03/2018.
 */

public class AdapterIdrekan extends RecyclerView.Adapter<AdapterIdrekan.LokerViewHolder> {

    String TAG = "Adapter_Loker";
    private List<ListIdrekan> listLoker;
    private Context context;

    public AdapterIdrekan(List<ListIdrekan> listLoker, Context context) {
        this.listLoker = listLoker;
        this.context = context;
    }

    @Override
    public LokerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.loker_list_row, parent, false);
        return new LokerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LokerViewHolder holder, final int position) {
        ListIdrekan model_loker = listLoker.get(position);

        final String id_perusahaan = model_loker.getId_perusahaan();
        final String nama_perusahaan = model_loker.getNama_perusahaan();
        final String deskripsi_perusahaan = model_loker.getDeskripsi_perusahaan();
        final String alamat_perusahaan = model_loker.getAlamat_perusahaan();
        final String bidang_perusahaan = model_loker.getBidang_perusahaan();

        final String id_lowongan = model_loker.getId_lowongan();
        final String posisi = model_loker.getPosisi_lowongan();
        final String id_bidang = model_loker.getId_bidang();
        final String nama_bidang = model_loker.getNama_bidang();
        final String syarat_umum = model_loker.getSyarat_umum();
        final String syarat_umur = model_loker.getSyarat_umur();
        final String pendidikan_jenjang = model_loker.getPendidikan_jenajang();
        final String pendidikan_prodi = model_loker.getPedidikan_prodi();
        final String syarat_skill = model_loker.getSyarat_skill();
        final String deskripsi_pekerjaan = model_loker.getDeskripsi_pekerjaan();
        final String gaji = model_loker.getGaji();
        final String cara_apply = model_loker.getCara_apply();
        final String syarat_dokumen = model_loker.getSyarat_dokumen();
        final String tgl_berakhir = model_loker.getTgl_berakhir();
        final String url_content = model_loker.getUrl_content();

        holder.tv_idLoker.setText(id_lowongan);
        holder.tv_posisi.setText(posisi);
        holder.tv_namaPerusahaan.setText(nama_perusahaan);
        holder.tv_lokasi.setText(gaji);
        holder.tv_batasWaktu.setText(tgl_berakhir);

        Log.e(TAG, "onBindViewHolder: id_perusahaan:" + id_perusahaan);
        Log.e(TAG, "onBindViewHolder: nama_perusahaan:" + nama_perusahaan);
        Log.e(TAG, "onBindViewHolder: deskripsi_perusahaan:" + deskripsi_perusahaan);
        Log.e(TAG, "onBindViewHolder: alamat_perusahaan:" + alamat_perusahaan);
        Log.e(TAG, "onBindViewHolder: bidang_perusahaan:" + bidang_perusahaan);

        Log.e(TAG, "onBindViewHolder: id_lowongan:" + id_lowongan);
        Log.e(TAG, "onBindViewHolder: posisi:" + posisi);
        Log.e(TAG, "onBindViewHolder: id_bidang:" + id_bidang);
        Log.e(TAG, "onBindViewHolder: nama_bidang:" + nama_bidang);
        Log.e(TAG, "onBindViewHolder: syarat_umum:" + syarat_umum);
        Log.e(TAG, "onBindViewHolder: syarat_umur:" + syarat_umur);
        Log.e(TAG, "onBindViewHolder: pendidikan_jenjang:" + pendidikan_jenjang);
        Log.e(TAG, "onBindViewHolder: pendidikan_prodi:" + pendidikan_prodi);
        Log.e(TAG, "onBindViewHolder: syarat_skill:" + syarat_skill);
        Log.e(TAG, "onBindViewHolder: deskripsi_pekerjaan:" + deskripsi_pekerjaan);
        Log.e(TAG, "onBindViewHolder: gaji:" + gaji);
        Log.e(TAG, "onBindViewHolder: cara_apply:" + cara_apply);
        Log.e(TAG, "onBindViewHolder: syarat_dokumen:" + syarat_dokumen);

        Log.e(TAG, "onBindViewHolder: tgl_berakhir:" + tgl_berakhir);
        Log.e(TAG, "onBindViewHolder: url_content:" + url_content);


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetailLoker.class);
                intent.putExtra("id_lowongan", id_lowongan);
                intent.putExtra("posisi", posisi);
                intent.putExtra("id_bidang", id_bidang);
                intent.putExtra("nama_bidang", nama_bidang);
                intent.putExtra("syarat_umum", syarat_umum);
                intent.putExtra("syarat_umur", syarat_umur);
                intent.putExtra("pendidikan_jenjang", pendidikan_jenjang);
                intent.putExtra("pendidikan_prodi", pendidikan_prodi);
                intent.putExtra("syarat_skill", syarat_skill);
                intent.putExtra("deskripsi_pekerjaan", deskripsi_pekerjaan);
                intent.putExtra("gaji", gaji);
                intent.putExtra("cara_apply", cara_apply);
                intent.putExtra("syarat_dokumen", syarat_dokumen);
                intent.putExtra("tgl_berakhir", tgl_berakhir);
                intent.putExtra("url_content", url_content);

                intent.putExtra("id_perusahaan", id_perusahaan);
                intent.putExtra("nama_perusahaan", nama_perusahaan);
                intent.putExtra("deskripsi_perusahaan", deskripsi_perusahaan);
                intent.putExtra("alamat_perusahaan", alamat_perusahaan);
                intent.putExtra("bidang_perusahaan", bidang_perusahaan);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listLoker.size();
    }

    public class LokerViewHolder extends RecyclerView.ViewHolder {

        TextView tv_idLoker, tv_posisi, tv_namaPerusahaan, tv_lokasi, tv_batasWaktu;

        CardView cardView;

        public LokerViewHolder(View itemView) {
            super(itemView);

            tv_idLoker = itemView.findViewById(R.id.tv_loker_id);
            tv_posisi = itemView.findViewById(R.id.tv_loker_posisi);
            tv_namaPerusahaan = itemView.findViewById(R.id.tv_loker_namaPerusahaan);
            tv_lokasi = itemView.findViewById(R.id.tv_loker_kategori_bidang);
            tv_batasWaktu = itemView.findViewById(R.id.tv_loker_deadline);
            cardView = itemView.findViewById(R.id.cardview_loker);
        }
    }
}
