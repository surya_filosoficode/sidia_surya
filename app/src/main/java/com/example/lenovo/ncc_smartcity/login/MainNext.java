package com.example.lenovo.ncc_smartcity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.internalLib.Session;
import com.example.lenovo.ncc_smartcity.internalLib.SessionCheck;

/**
 * Created by USER on 23/03/2018.
 */

public class MainNext extends FragmentActivity {
    Button btnLogout;
    TextView txt_username, txt_id;
    String TAG = "MainNext";
    Intent intent;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main_next);

        btnLogout = (Button) findViewById(R.id.btn_logout);
        txt_username = (TextView) findViewById(R.id.txt_username);
        txt_id = (TextView) findViewById(R.id.txt_id_user);

        String[] data_session = new SessionCheck().seesionLoginChecker(MainNext.this);
        Log.e(TAG, "onClick: " + data_session);
        if (data_session != null) {
            Log.e(TAG, "onClick: " + data_session[0]);

            if (Boolean.valueOf(data_session[0])) {
                txt_username.setText(data_session[1]);
                txt_id.setText(data_session[2]);
                Log.e(TAG, "onCreate: " + data_session[3]);
            }
        }
//        Toast.makeText(MainNext.this, statusLogin, Toast.LENGTH_SHORT).show();


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Session().sessionDestroy(MainNext.this);

                intent = new Intent(MainNext.this, MainLogin.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
