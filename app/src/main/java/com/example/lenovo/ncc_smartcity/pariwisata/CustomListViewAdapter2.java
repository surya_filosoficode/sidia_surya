package com.example.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by lenovo on 2/13/2018.
 */

public class CustomListViewAdapter2 extends BaseAdapter {

    private final Activity context;
    private final ArrayList<String> titles;
    private final ArrayList<String> alamat;
    private final ArrayList<String> deskripsi;
    private final ArrayList<String> telepon;
    private final ArrayList<String> gambar;
    private String pilihan;

    public CustomListViewAdapter2(Activity context
            , ArrayList<String> titles
            , ArrayList<String> alamat
            , ArrayList<String> deskripsi
            , ArrayList<String> telepon
            , ArrayList<String> gambar
            , String pilihan) {
        this.context = context;
        this.titles = titles;
        this.alamat = alamat;
        this.deskripsi = deskripsi;
        this.telepon = telepon;
        this.gambar = gambar;
        this.pilihan = pilihan;

    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return titles.size();
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Get the row id associated with the specified position in the Layanan_sublist.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link LayoutInflater#inflate(int, ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.pariwisata_customlistview_customrow, null);
        }

        TextView tv_title = rowView.findViewById(R.id.list_title);

        ImageView iv_icon = rowView.findViewById(R.id.list_image);

        tv_title.setText(this.titles.get(position));

//        Picasso.with(rowView.getContext()).load("http://192.168.86.25/sidia/assets/source/wisata/" + pilihan + "/" + gambar.get(position) + ".jpg").into(iv_icon);
        Picasso.with(rowView.getContext()).load("http://sidia.malangkota.go.id/assets/source/wisata/"+pilihan+"/"+gambar.get(position)+".jpg").into(iv_icon);
//https://keberhasilansmart-yusron.c9users.io/smartcity/gambarwisata/hotel/hotel_1
        CardView cardViewHotel = rowView.findViewById(R.id.cardView_listHotel);
        cardViewHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(v.getContext(), SingleMenu.class);
                //If you wanna send any data to nextActicity.class you can use
                i.putExtra("id", position);
                i.putExtra("selected", pilihan);
                i.putExtra("titles", titles.get(position));
                i.putExtra("alamat", alamat.get(position));
                i.putExtra("deskripsi", deskripsi.get(position));
                i.putExtra("telepon", telepon.get(position));
//                i.putExtra("gambar", "https://keberhasilansmart-yusron.c9users.io/smartcity/gambarwisata/" + pilihan + "/");
          i.putExtra("gambar", "http://sidia.malangkota.go.id/assets/source/wisata/"+pilihan+"/"+gambar.get(position)+".jpg");
                v.getContext().startActivity(i);
            }
        });
//lucu
        Button panda = (Button) rowView.findViewById(R.id.list_lihat);
        panda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), SingleMenu.class);
                //If you wanna send any data to nextActicity.class you can use
                i.putExtra("id", position);
                i.putExtra("selected", pilihan);
                i.putExtra("id", position);
                i.putExtra("selected", pilihan);
                i.putExtra("titles", titles.get(position));
                i.putExtra("alamat", alamat.get(position));
                i.putExtra("deskripsi", deskripsi.get(position));
                i.putExtra("telepon", telepon.get(position));

                //http://sidia.malangkota.go.id/assets/source/wisata/"+pilihan+/
                i.putExtra("gambar", "http://sidia.malangkota.go.id/assets/source/wisata/"+pilihan+"/"+gambar.get(position)+".jpg");
//                i.putExtra("gambar", "https://keberhasilansmart-yusron.c9users.io/smartcity/gambarwisata/" + pilihan + "/");
                view.getContext().startActivity(i);
            }
        });
        return rowView;
    }
}
