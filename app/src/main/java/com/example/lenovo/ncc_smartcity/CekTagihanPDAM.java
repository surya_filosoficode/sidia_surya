package com.example.lenovo.ncc_smartcity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTagihan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CekTagihanPDAM extends AppCompatActivity {
    MessageVariabe messageVariabe = new MessageVariabe();
    String TAG = "CekTagihanPDAM",
            BASE_URL = URLCollection.BASE_URL_FOTO,
            TOKEN_MOBILE = "X00W";

    Call<ResponseBody> get_data;
    SendDataTagihan base_url_management;

    CheckConn checkConn = new CheckConn();

    Button btn_hapus, btn_cekTagihan;

    LinearLayout ll_hasilCekTagihan;

    EditText et_nomorTagihan;

    TextView tvNama, tvAlamat, tvNosal, tvGolongan, tvTagihan, tvStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_tagihan_pdam);

        et_nomorTagihan = findViewById(R.id.et_nomorTagihanPDAM);

        ll_hasilCekTagihan = findViewById(R.id.ll_cekTagihanHasil);

        tvNama  = (TextView) findViewById(R.id.tv_nama);
        tvAlamat = (TextView) findViewById(R.id.tv_alamat);
        tvNosal = (TextView) findViewById(R.id.tv_nosal);
        tvGolongan = (TextView) findViewById(R.id.tv_golongan);
        tvTagihan = (TextView) findViewById(R.id.tv_tagihan);
        tvStatus = (TextView) findViewById(R.id.tv_status);


        btn_cekTagihan = findViewById(R.id.btn_cekTagihan);
        btn_cekTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_hasilCekTagihan.setVisibility(View.VISIBLE);
                if (checkConn.isConnected(CekTagihanPDAM.this)) {
                    base_url_management = Base_url.getClient_notGson(BASE_URL).create(SendDataTagihan.class);
                    get_data = base_url_management.getDataPDAM(TOKEN_MOBILE, et_nomorTagihan.getText().toString());

                    set_data_pdam(get_data);
                }
            }
        });

        btn_hapus = findViewById(R.id.btn_hapus);
        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_nomorTagihan.setText("");
            }
        });
    }


    private void set_data_pdam(Call<ResponseBody> getdata) {
        Log.e(TAG, "set_data_pdam: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    Log.e(TAG, "set_data_pdam: "+ response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                try {
//                    JSONObject data_json = null;
//                    String surya = "{\"\"}";
//                    data_json = new JSONObject(response.body().string().toString());
//
//                    String status = data_json.getJSONObject("body_message").getString("status");
//                    if (Boolean.valueOf(status)) {
//                        JSONObject main_data = new JSONObject(data_json.getString("body").toString());
//                        String status_get_pdam = main_data.getString("success");
//
//                        Log.e(TAG, "set_data_pdam: "+ main_data);
//
//                        if (Boolean.valueOf(status_get_pdam)) {
//                            JSONArray data_plg = new JSONArray(main_data.getJSONObject("plg").getJSONArray("tagihan").toString());
//                            String tagihan = "";
//                            Log.e(TAG, "onResponse: " + tagihan);
//                            Locale localeID = new Locale("in", "ID");
//                            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(localeID);
//                            for (int i = 0; i < data_plg.length(); i++) {
//                                JSONObject data_tmp = new JSONObject(data_plg.get(i).toString());
//                                String total = data_tmp.getString("total");
//                                String periode = data_tmp.getString("periode");
//
//                                String th_periode = periode.substring(0, 4);
//                                String bln_periode = periode.substring(4, 6);
//
//                                Log.e(TAG, "onResponse: " + th_periode);
//                                Log.e(TAG, "onResponse: " + bln_periode);
//                                tagihan += numberFormat.format((double) Double.valueOf(total)).toString() + ", " + th_periode + " - " + bln_periode + " \n";
//                            }
//
//                            String golongan = main_data.getJSONObject("plg").getString("golongan");
//                            String nosal = main_data.getJSONObject("plg").getString("nosal");
//                            String nama = main_data.getJSONObject("plg").getString("nama");
//                            String alamat = main_data.getJSONObject("plg").getString("alamat");
//                            String status_bayar = main_data.getJSONObject("plg").getString("status");
//
//                            tvTagihan.setText(tagihan);
//                            tvNosal.setText(nosal);
//                            tvNama.setText(nama);
//                            tvAlamat.setText(alamat);
//                            tvGolongan.setText(golongan);
//                            tvStatus.setText(status_bayar);
//                        }
//                    }
//                    String message = data_json.getJSONObject("body_message").getString("message");
//                    Toast.makeText(CekTagihanPDAM.this, message, Toast.LENGTH_LONG).show();
//                } catch (IOException e) {
//                    Toast.makeText(CekTagihanPDAM.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID, Toast.LENGTH_LONG).show();
//                    Log.e(TAG, "onResponse: IOException: " + e.toString());
//                } catch (JSONException e) {
//                    Toast.makeText(CekTagihanPDAM.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID, Toast.LENGTH_LONG).show();
//                    Log.e(TAG, "onResponse: JSONException: " + e.getMessage().toString());
//                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CekTagihanPDAM.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID, Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

}
