package com.example.lenovo.ncc_smartcity.event;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.example.lenovo.ncc_smartcity.R;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

/**
 * Created by arimahardika on 11/04/2018.
 */

public class Event_New_Layout extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout, tab_sembako, tab_panen;
    MaterialSearchView searchView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_new_display);

        initializeVariable();
    }

    private void initializeVariable() {
        viewPager = findViewById(R.id.pasar_viewPager);
        tabLayout = findViewById(R.id.pasar_tabLayout);

        tab_sembako = findViewById(R.id.pasar_tabSembako);
        tab_panen = findViewById(R.id.pasar_tabKebutuhanPanen);


        PageAdapterEvent pageAdapter = new PageAdapterEvent(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        Toolbar toolbar = findViewById(R.id.pasar_new_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        //set on tab selected
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        searchView = findViewById(R.id.pasar_new_search);

        //disableSelectTab();
    }

    private void disableSelectTab() {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.pasar_options, menu);
//        MenuItem item = menu.findItem(R.id.menu_main_search);
//        return true;
//
//    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {


        finish();
    }
}
