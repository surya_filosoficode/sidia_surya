package com.example.lenovo.ncc_smartcity.event.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.event.CustomAdapterEvent;
import com.example.lenovo.ncc_smartcity.event.DataModel;
import com.example.lenovo.ncc_smartcity.main_feature.Event;
import com.example.lenovo.ncc_smartcity.pasar.Adapter_pasar;
import com.example.lenovo.ncc_smartcity.pasar.Model_Pasar;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventMalang extends Fragment {
    RecyclerView recyclerView;
    Adapter_pasar adapter_pasar;
    ArrayList<DataModel> dataModels;
    int searchSelesai = 0;
    ProgressDialog progressDialog;
    TextView kosong ;
    String loadingMessage = "Loading";
    LinearLayout ll_sumber_pasar;
    MaterialSearchView searchView;

    public EventMalang() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.event_fragment, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
        ll_sumber_pasar = view.findViewById(R.id.ll_sumber_pasar);
        recyclerView = view.findViewById(R.id.event_recyclerView);

        recyclerView.hasFixedSize();
        dataModels = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));


        listSemuaEvent(view);

    }

    public void listSemuaEvent(final View v) {
        RequestQueue queue = Volley.newRequestQueue(v.getContext());
        String url = "http://sidia.malangkota.go.id/smartcity/smart/eventmalangnet";

        progressDialog = new ProgressDialog(v.getContext()) {
            @Override
            public void onBackPressed() {
                getActivity().finish();
            }
        };
        //picolo
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                        Log.e("apa", response);
//                    progressDialog.dismiss();

                        try {
                            dataModels = new ArrayList<>();
                            JSONArray panda = new JSONArray(response);
                            for (int i = 0; i < panda.length(); i++) {
                                JSONObject panda1 = panda.getJSONObject(i);
                                //image dah jadi besar kalau dishare
//                                String nama = "";
//                                for (int j = 0; j < panda1.getJSONArray("category").length(); j++) {
//
//                                    nama += panda1.getJSONArray("category").getJSONObject(j).getString("nama") + ",";
////                                    String link= panda1.getJSONArray("category").getString("nama");
//                                }
//                                nama.substring(0, nama.length() - 1);
                                dataModels.add(new DataModel(panda1.getString("img").replace("-150x150", ""),
                                        panda1.getString("link"),
                                        Jsoup.parse(panda1.getString("title")).text(),
                                        Jsoup.parse(panda1.getString("description")).text(),
                                        "Category : "));

                            }
//                           adapter= new CustomAdapter(dataModels,getApplicationContext());
//
//                            listView.setAdapter(adapter);
                            recyclerView = (RecyclerView) v.findViewById(R.id.event_recyclerView);

                            CustomAdapterEvent adapter = new CustomAdapterEvent(dataModels);

                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(v.getContext());

                            recyclerView.setLayoutManager(layoutManager);

                            recyclerView.setAdapter(adapter);
                            progressDialog.dismiss();
                            //mTextView.setText(panda1.getString("img"));
                        } catch (JSONException e) {
                            Log.e("panda", e.toString());
                            Toast.makeText(v.getContext(), "Error, something went wrong!", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                        // Getting JSON Array node

//                        mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mTextView.setText("That didn't work!");
                Log.e("apa", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

}
