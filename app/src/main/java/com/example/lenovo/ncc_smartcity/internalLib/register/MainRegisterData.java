package com.example.lenovo.ncc_smartcity.internalLib.register;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by USER on 02/04/2018.
 */

public class MainRegisterData {
    String TAG = "MainRegisterGet";

    public String[] MainRegisterGet(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("mainReg", Context.MODE_PRIVATE);
        Log.e(TAG, "MainRegisterGet: " + prefs);
        if (prefs != null) {
            if (prefs.getBoolean("status_akses", false)) {
                String nik = prefs.getString("nik", null);
                String email = prefs.getString("email", null);//"No name defined" is the default value.
                String username = prefs.getString("username", null); //0 is the default value.
                String tlp = prefs.getString("tlp", null);
                String status_warga = prefs.getString("status_warga", null); //0 is the default value.
                String pass = prefs.getString("pass", null);

                Log.e(TAG, "MainRegisterGet: " + nik);
                String[] session_val = {nik, username, email, tlp, status_warga, pass};
                return session_val;
            }
        }

        return null;
    }


    public void MainRegisterSet(Context context, String nik, String username, String tlp,
                                String email, String status_warga, String pass) {
        Log.e(TAG, "MainRegisterSet: run");
        try {
            Log.e(TAG, "MainRegisterSet: run");
            SharedPreferences sharedPref = context.getSharedPreferences("mainReg", Context.MODE_PRIVATE);
            Log.e(TAG, "MainRegisterSet: sharedPref: " + sharedPref);
            SharedPreferences.Editor editor = sharedPref.edit();
            Log.e(TAG, "MainRegisterSet: editor: " + editor);

//        editor.putString("name", "Elen);
            editor.putString("email", email);
            editor.putString("nik", nik);
            editor.putString("username", username);
            editor.putString("tlp", tlp);
            editor.putString("status_warga", status_warga);
            editor.putString("pass", pass);

            editor.putBoolean("status_akses", true);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, "MainRegisterSet: " + e.getMessage());
        }
    }

    public void MainRegisterDestroy(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("mainReg", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Log.e(TAG, "sessionDestroy: session destroy");
    }
}
