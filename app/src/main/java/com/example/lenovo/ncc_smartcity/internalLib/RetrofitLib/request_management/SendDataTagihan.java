package com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by USER on 21/06/2018.
 */

public interface SendDataTagihan {

    @FormUrlEncoded
    @POST("pdam/checkpdam/checkTagihan")
    Call<ResponseBody> getDataPDAM(@Field("token") String token,
                                   @Field("nosal") String nosal);
}
