package com.example.lenovo.ncc_smartcity.lowongan_pekerjaan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;

import at.blogc.android.views.ExpandableTextView;

/**
 * Created by USER on 05/06/2018.
 */

public class DetailLoker extends AppCompatActivity {
    String TAG = "DetailLoker";

//
//    List<String> list_syarat_umum = new ArrayList<>();
//    List<String> list_syarat_jenjang = new ArrayList<>();
//    List<String> list_syarat_prodi = new ArrayList<>();
//    List<String> list_syarat_dokumen = new ArrayList<>();
//    List<String> list_skill = new ArrayList<>();
//
//    ListView lv_syarat_umum,
//                lv_syarat_jenjang,
//                lv_syarat_prodi,
//                lv_syarat_dokumen,
//                lv_skill;
//
//    ArrayAdapter<String> adapter_syarat_umum,
//            adapter_syarat_jenjang,
//            adapter_syarat_prodi,
//            adapter_syarat_dokumen,
//            adapter_syarat_skill;
//
//    TextSafety textSafety = new TextSafety();

    TextView tv_posisi, tv_nama_bidang, tv_syarat_umur,
            tv_pendidikan_jenjang, tv_pendidikan_prodi, tv_syarat_skill, tv_deskripsi_pekerjaan,
            tv_gaji, tv_syarat_dokumen, tv_tgl_berakhir, tv_url_content,
            tv_nama_perusahaan, tv_alamat_perusahaan, tv_bidang_perusahaan;

    String id_lowongan, posisi, id_bidang, nama_bidang, syarat_umum,
            syarat_umur, pendidikan_jenjang, pendidikan_prodi, syarat_skill,
            deskripsi_pekerjaan, gaji, cara_apply, syarat_dokumen, tgl_berakhir,
            url_content, id_perusahaan, nama_perusahaan, deskripsi_perusahaan,
            alamat_perusahaan, bidang_perusahaan;

    ExpandableTextView expandable_tv_syarat_umum, expandable_tv_cara_mendaftar,
            expandable_tv_deskripsi_perusahaan;

    ImageButton expandable_imgbtn_syarat_umum, expandable_imgbtn_cara_mendaftar,
            expandable_imgbtn_deskripsi_perusahaan;

    String modified_syarat_umum, modified_jenjang_pendidikan, modified_prodi, modified_skill,
            modified_dokumen, modified_alamat, modified_bidang_perusahaan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idrekan_main);

//        lv_syarat_umum = (ListView) findViewById(R.id.list_syarat);
//        lv_syarat_jenjang = (ListView) findViewById(R.id.list_jenjang_pendidikan);
//        lv_syarat_prodi = (ListView) findViewById(R.id.list_pendidikan_prodi);
//        lv_skill = (ListView) findViewById(R.id.list_syarat_skill);
//        lv_syarat_dokumen = (ListView) findViewById(R.id.list_syarat_dokumen);

        getBundle();

//        String[] data = syarat_umum.split(";");
//
//        adapter_syarat_umum = new ArrayAdapter<String>(this,
//                R.layout.idrekan_list_syarat_umum,
//                R.id.tv_syarat_umum,
//                data);
//        lv_syarat_umum.setAdapter(adapter_syarat_umum);
//
//        String[] data_jenjang = pendidikan_jenjang.split(";");
//
//        String[] data_prodi = pendidikan_prodi.split(";");
//
//        String[] data_dokumen = syarat_dokumen.split(";");
//
//        String[] skill = syarat_skill.split(";");

        modifyData();

        findView();

        assignData();

        expandableSetting();
    }

    private void expandableSetting() {
        expandable_tv_syarat_umum.setInterpolator(new AccelerateDecelerateInterpolator());
        expandable_tv_cara_mendaftar.setInterpolator(new AccelerateDecelerateInterpolator());
        expandable_tv_deskripsi_perusahaan.setInterpolator(new AccelerateDecelerateInterpolator());

        expandable_imgbtn_syarat_umum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandable_tv_syarat_umum.isExpanded()) {
                    expandable_imgbtn_syarat_umum.setImageResource(R.drawable.ic_keyboard_arrow_down_48dp);
                    expandable_tv_syarat_umum.toggle();
                } else {
                    expandable_imgbtn_syarat_umum.setImageResource(R.drawable.ic_keyboard_arrow_up_48dp);
                    expandable_tv_syarat_umum.toggle();
                }
            }
        });

        expandable_imgbtn_cara_mendaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandable_tv_cara_mendaftar.isExpanded()) {
                    expandable_imgbtn_cara_mendaftar.setImageResource(R.drawable.ic_keyboard_arrow_down_48dp);
                    expandable_tv_cara_mendaftar.toggle();
                } else {
                    expandable_imgbtn_cara_mendaftar.setImageResource(R.drawable.ic_keyboard_arrow_up_48dp);
                    expandable_tv_cara_mendaftar.toggle();
                }
            }
        });

        expandable_imgbtn_deskripsi_perusahaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandable_tv_deskripsi_perusahaan.isExpanded()) {
                    expandable_imgbtn_deskripsi_perusahaan.setImageResource(R.drawable.ic_keyboard_arrow_down_48dp);
                    expandable_tv_deskripsi_perusahaan.toggle();
                } else {
                    expandable_imgbtn_deskripsi_perusahaan.setImageResource(R.drawable.ic_keyboard_arrow_up_48dp);
                    expandable_tv_deskripsi_perusahaan.toggle();
                }
            }
        });

    }

    private void modifyData() {
        modified_syarat_umum = syarat_umum.replace(";", "\n");
        modified_jenjang_pendidikan = pendidikan_jenjang.replace(";", "\n");
        modified_prodi = pendidikan_prodi.replace(";", "\n");
        modified_skill = syarat_skill.replace(";", "\n");
        modified_dokumen = syarat_dokumen.replace(";", "\n");
        modified_alamat = alamat_perusahaan.replace("||", "\n");
        modified_bidang_perusahaan = bidang_perusahaan.replace("||", "\n");
    }

    private void getBundle() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        id_lowongan = bundle.getString("id_lowongan");
        posisi = bundle.getString("posisi");
        id_bidang = bundle.getString("id_bidang");
        nama_bidang = bundle.getString("nama_bidang");
        syarat_umum = bundle.getString("syarat_umum");
        syarat_umur = bundle.getString("syarat_umur");
        pendidikan_jenjang = bundle.getString("pendidikan_jenjang");
        pendidikan_prodi = bundle.getString("pendidikan_prodi");
        syarat_skill = bundle.getString("syarat_skill");
        deskripsi_pekerjaan = bundle.getString("deskripsi_pekerjaan");
        gaji = bundle.getString("gaji");
        cara_apply = bundle.getString("cara_apply");
        syarat_dokumen = bundle.getString("syarat_dokumen");
        tgl_berakhir = bundle.getString("tgl_berakhir");
        url_content = bundle.getString("url_content");

        id_perusahaan = bundle.getString("id_perusahaan");
        nama_perusahaan = bundle.getString("nama_perusahaan");
        deskripsi_perusahaan = bundle.getString("deskripsi_perusahaan");
        alamat_perusahaan = bundle.getString("alamat_perusahaan");
        bidang_perusahaan = bundle.getString("bidang_perusahaan");

//        Log.e(TAG, "onCreate: "+nama_perusahaan);
//
//        Log.e(TAG, "onCreate: "+pendidikan_jenjang);
//        Log.e(TAG, "onCreate: "+";".getBytes(StandardCharsets.US_ASCII).toString());
//
//        if(textSafety.get_valid_regex("surya;dinda")){
//            Log.e(TAG, "onCreate: ooh yeah");
//        }
//
//        if(textSafety.get_valid_regex("aasss dda")){
//            Log.e(TAG, "onCreate: ooh yeah");
//        }

    }

    private void findView() {
//        tv_id_lowongan = findViewById(R.id.tv_id_lowongan);
        tv_posisi = findViewById(R.id.tv_posisi);
        tv_nama_bidang = findViewById(R.id.tv_nama_bidang);
//        tv_syarat_umum = findViewById(R.id.tv_syarat_umum);
        tv_syarat_umur = findViewById(R.id.tv_syarat_umur);

        tv_pendidikan_jenjang = findViewById(R.id.tv_pendidikan_jenjang);
        tv_pendidikan_prodi = findViewById(R.id.tv_pendidikan_prodi);
        tv_syarat_skill = findViewById(R.id.tv_syarat_skill);
        tv_deskripsi_pekerjaan = findViewById(R.id.tv_desc_pekerjaan);

        tv_gaji = findViewById(R.id.tv_gaji);
//        tv_cara_apply = findViewById(R.id.tv_cara_apply);
        tv_syarat_dokumen = findViewById(R.id.tv_syarat_dokumen);
        tv_tgl_berakhir = findViewById(R.id.tv_tgl_berakhir);
        tv_url_content = findViewById(R.id.tv_url_content);

        tv_nama_perusahaan = findViewById(R.id.tv_nama_perusahaan);
//        tv_deskripsi_perusahaan = findViewById(R.id.tv_desc_perusahaan);
        tv_alamat_perusahaan = findViewById(R.id.tv_alamat);
        tv_bidang_perusahaan = findViewById(R.id.tv_bidang_perusahaan);

        expandable_tv_syarat_umum = findViewById(R.id.expandable_tv_syarat_umum);
        expandable_tv_cara_mendaftar = findViewById(R.id.expandable_tv_cara_mendaftar);
        expandable_tv_deskripsi_perusahaan = findViewById(R.id.expandable_tv_deskripsi_perusahaan);

        expandable_imgbtn_syarat_umum = findViewById(R.id.imgbtn_expandable_syarat_umum);
        expandable_imgbtn_cara_mendaftar = findViewById(R.id.imgbtn_expandable_cara_mendaftar);
        expandable_imgbtn_deskripsi_perusahaan = findViewById(R.id.imgbtn_expandable_deskripsi_perusahaan);
    }

    private void assignData() {
//        tv_id_lowongan.setText("ID Lowongan : " + id_lowongan);
        tv_posisi.setText(posisi);
        tv_nama_bidang.setText(nama_bidang);
//        tv_syarat_umum.setText(modified_syarat_umum);
        tv_syarat_umur.setText(syarat_umur);

        tv_pendidikan_jenjang.setText(modified_jenjang_pendidikan);
        tv_pendidikan_prodi.setText(modified_prodi);
        tv_syarat_skill.setText(modified_skill);
        tv_deskripsi_pekerjaan.setText(deskripsi_pekerjaan);

        tv_gaji.setText(gaji);
//        tv_cara_apply.setText(cara_apply);
        tv_syarat_dokumen.setText(modified_dokumen);
        tv_tgl_berakhir.setText(tgl_berakhir);
        tv_url_content.setText("URL CONTENT");

        tv_nama_perusahaan.setText(nama_perusahaan);
//        tv_deskripsi_perusahaan.setText(deskripsi_perusahaan);
        tv_alamat_perusahaan.setText(modified_alamat);
        tv_bidang_perusahaan.setText(modified_bidang_perusahaan);

        expandable_tv_syarat_umum.setText(modified_syarat_umum);
        expandable_tv_cara_mendaftar.setText(cara_apply);
        expandable_tv_deskripsi_perusahaan.setText(deskripsi_perusahaan);
    }

}
