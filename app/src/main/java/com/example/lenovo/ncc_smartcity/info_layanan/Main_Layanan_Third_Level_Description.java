package com.example.lenovo.ncc_smartcity.info_layanan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity.info_layanan.Utilities.Adapter_Deskripsi_Layanan;
import com.example.lenovo.ncc_smartcity.info_layanan.Utilities.Adapter_Layanan;
import com.example.lenovo.ncc_smartcity.info_layanan.alur_layanan.AlurLayanan;

/**
 * Created by arimahardika on 23/03/2018.
 */

public class Main_Layanan_Third_Level_Description extends Activity {

    Adapter_Layanan adapterLayanan;
    Adapter_Deskripsi_Layanan adapter_deskripsi_layanan;
    RecyclerView recyclerView;
    TextView textView, sumber, subtittle;
    CharSequence[] listLayanan;
    String selectedFeature;
    Button button;
    Typeface customFont;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layanan_third_level_description);

        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        textView = findViewById(R.id.tv_layanan_description);
        textView.setTypeface(customFont);
        sumber = findViewById(R.id.sumber_dispenduk);
        subtittle = findViewById(R.id.sub_tittle);

        button = findViewById(R.id.btn_alur);

        recyclerView = findViewById(R.id.layanan_recycler_3rd_level);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getExtra();

        showListBasedOnCategory(selectedFeature);
    }

    private void getDataBasedOnCategory(int title, int res_array, String category,
                                        String txt_sumber, String txt_subtitle) {
        textView.setText(title);
        listLayanan = getResources().getTextArray(res_array);

        if (title == R.string.pelayananPajak) {
            adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
            recyclerView.setAdapter(adapterLayanan);
        } else if (title == R.string.info_instansi ||
                title == R.string.dinas_kesehatan) {
            adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
            button.setVisibility(View.GONE);
            recyclerView.setAdapter(adapterLayanan);
        } else {
            adapter_deskripsi_layanan = new Adapter_Deskripsi_Layanan(listLayanan, getApplicationContext());
            recyclerView.setAdapter(adapter_deskripsi_layanan);
        }

        buttonClick(category);

        sumber.setText(txt_sumber);
        subtittle.setText(txt_subtitle);
    }

    private void buttonClick(final String category) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Layanan_Third_Level_Description.this, AlurLayanan.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, category);
                i.putExtra(StaticVariable.SPECIFIC_FEATURE, subtittle.getText());
                startActivity(i);
            }
        });
    }

    private void showListBasedOnCategory(String category) {
        switch (category) {

            //<editor-fold desc="KEPENDUDUKAN">

            //<editor-fold desc="KARTU KELUARGA">
            case StaticVariable.KK_BARU:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kk_baru,
                        StaticVariable.KARTU_KELUARGA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KK_BARU);

                break;

            case StaticVariable.KK_HILANG_RUSAK:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kk_hilang_rusak,
                        StaticVariable.KARTU_KELUARGA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KK_HILANG);
                break;

            case StaticVariable.KK_TAMBAH_ANGGOTA:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kk_tambahAnggota,
                        StaticVariable.KARTU_KELUARGA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KK_TAMBAH_ANGGOTA);
                break;

            case StaticVariable.KK_PENGURANGAN_ANGGOTA:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kk_penguranganAnggota,
                        StaticVariable.KARTU_KELUARGA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KK_PENGURANGAN_ANGGOTA);
                break;

            case StaticVariable.KK_PERUBAHAN_ANGGOTA:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kk_perubahanAnggota,
                        StaticVariable.KARTU_KELUARGA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KK_PERUBAHAN_ANGGOTA);
                break;
            //</editor-fold>

            //<editor-fold desc="AKTA">
            case StaticVariable.AKTA_KELAHIRAN:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.akta_kelahiran,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_KELAHIRAN);
                break;

            case StaticVariable.AKTA_KEMATIAN:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.akta_kematian,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_KEMATIAN);
                break;

            case StaticVariable.AKTA_PERKAWINAN:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.akta_perkawinan,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_PERKAWINAN);
                break;

            case StaticVariable.AKTA_PENCATATAN_PERUBAHAN_NAMA:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.perubahan_nama,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_PENCATATAN_PERUBAHAN_NAMA);
                break;

            case StaticVariable.AKTA_PENCATATAN_PENGAKUAN_ANAK:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.pengakuan_anak,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_PENCATATAN_PENGAKUAN_ANAK);
                break;

            case StaticVariable.KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.kutipan_kedua_akta_pencatatan_sipil,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL);
                break;

            case StaticVariable.AKTA_ADOPSI:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.adopsi,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_PENCATATAN_PENGANGKATAN_ANAK);
                break;

            case StaticVariable.AKTA_PENGESAHAN_ANAK:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.akta_pengesahan_anak,
                        StaticVariable.AKTA, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_AKTA_PENGESAHAN_ANAK);
                break;
            //</editor-fold>

            //<editor-fold desc="KTP">
            case StaticVariable.KTP_BARU:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.ktp_baru,
                        StaticVariable.KTP, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KTP_BARU);
                break;

            case StaticVariable.KTP_HILANG_RUSAK:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.ktp_hilang_rusak,
                        StaticVariable.KTP, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KTP_HILANG);
                break;

            case StaticVariable.KTP_SALAH_DIRUBAH:
                getDataBasedOnCategory(R.string.pelayananKependudukan, R.array.ktp_salah_dirubah,
                        StaticVariable.KTP, StaticVariable.SUMBER_DISPENDUK,
                        StaticVariable.SUBTITLE_KTP_SALAH);
                break;
            //</editor-fold>

            //</editor-fold>

            //<editor-fold desc="PERIZINAN">
            case StaticVariable.IMB:
                getDataBasedOnCategory(R.string.pelayananPerizinan, R.array.imb,
                        StaticVariable.PERIZINAN, StaticVariable.SUMBER_PERIJINAN,
                        StaticVariable.SUBTITLE_PERIJINAN_IMB);
                break;
            case StaticVariable.IZIN_GANGGUAN:
                getDataBasedOnCategory(R.string.pelayananPerizinan, R.array.izin_gangguan,
                        StaticVariable.PERIZINAN, StaticVariable.SUMBER_PERIJINAN,
                        StaticVariable.SUBTITLE_PERIJINAN_IZIN_GANGGUAN);
                break;
            case StaticVariable.IZIN_TOKO_MODERN:
                getDataBasedOnCategory(R.string.pelayananPerizinan, R.array.izin_toko_modern,
                        StaticVariable.PERIZINAN, StaticVariable.SUMBER_PERIJINAN,
                        StaticVariable.SUBTITLE_PERIJINAN_IZIN_TOKO);
                break;
            case StaticVariable.IZIN_TONTONAN:
                getDataBasedOnCategory(R.string.pelayananPerizinan, R.array.izin_tontonan,
                        StaticVariable.PERIZINAN, StaticVariable.SUMBER_PERIJINAN,
                        StaticVariable.SUBTITLE_PERIJINAN_TONTONAN);
                break;
            case StaticVariable.SIUP:
                getDataBasedOnCategory(R.string.pelayananPerizinan, R.array.siup,
                        StaticVariable.PERIZINAN, StaticVariable.SUMBER_PERIJINAN,
                        StaticVariable.SUBTITLE_PERIJINAN_SIUP);
                break;
            //</editor-fold>

            case StaticVariable.PAJAK_HOTEL:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_hotel,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_HOTEL);
                break;

            case StaticVariable.PAJAK_RESTORAN:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_restoran,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_RESTORAN);
                break;

            case StaticVariable.PAJAK_HIBURAN:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_hiburan,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_HIBURAN);
                break;

            case StaticVariable.PAJAK_REKLAME:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_reklame,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_REKLAME);
                break;

            case StaticVariable.PAJAK_PENERANGAN_JALAN:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_penerangan_jalan,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_PENERANGAN_JALAN);
                break;

            case StaticVariable.PAJAK_PARKIR:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_parkir,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_PARKIR);
                break;

            case StaticVariable.PAJAK_AIR_TANAH:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_air_tanah,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_AIR_TANAH);
                break;

            case StaticVariable.BPHTB:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.bphtb,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_BPHTB);
                break;

            case StaticVariable.PAJAK_BUMI_BANGUNAN:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.pajak_bumi_bangunan,
                        StaticVariable.PAJAK, StaticVariable.SUMBER_PAJAK,
                        StaticVariable.SUBTITLE_PAJAK_BUMI);
                break;
            //</editor-fold>

            case StaticVariable.INSTANSI:
                getDataBasedOnCategory(R.string.info_instansi, R.array.instansi_alamat,
                        StaticVariable.INSTANSI, StaticVariable.EMPTY, StaticVariable.EMPTY);
                break;

            case StaticVariable.DINAS_KESEHATAN:
                getDataBasedOnCategory(R.string.dinas_kesehatan, R.array.alamat_dinas_kesehatan,
                        StaticVariable.DINAS_KESEHATAN, StaticVariable.EMPTY, StaticVariable.EMPTY);
                break;

            case StaticVariable.PELAYANAN_PAJAK:
                getDataBasedOnCategory(R.string.pelayananPajak, R.array.alamat_badan_pelayanan_pajak,
                        StaticVariable.PELAYANAN_PAJAK, StaticVariable.EMPTY, StaticVariable.EMPTY);
                break;
        }
    }


    private void getExtra() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
        }
    }
}
