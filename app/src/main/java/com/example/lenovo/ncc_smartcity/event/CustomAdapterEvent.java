package com.example.lenovo.ncc_smartcity.event;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Dimas Maulana on 5/26/17.
 * Email : araymaulana66@gmail.com
 */

public class CustomAdapterEvent extends RecyclerView.Adapter<CustomAdapterEvent.MahasiswaViewHolder> {

    private ArrayList<DataModel> dataList;

    public CustomAdapterEvent(ArrayList<DataModel> dataList) {
        this.dataList = dataList;
    }

    static public void shareImage(String url, final String teks, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_TEXT, teks);
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    static public Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_.png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    // Returns the URI path to the Bitmap displayed in specified ImageView
//    public Uri getLocalBitmapUri(ImageView imageView) {
//        // Extract Bitmap from ImageView drawable
//        Drawable drawable = imageView.getDrawable();
//        Bitmap bmp = null;
//        if (drawable instanceof BitmapDrawable) {
//            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
//        } else {
//            return null;
//        }
//        // Store image to default external storage directory
//        Uri bmpUri = null;
//        try {
//
//            File file = new File(Environment.getExternalStoragePublicDirectory(
//                    Environment.DIRECTORY_DOWNLOADS), "share_image.png");
//            file.getParentFile().mkdirs();
//            FileOutputStream out = new FileOutputStream(file);
//            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
//            out.close();
//            bmpUri = Uri.fromFile(file);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return bmpUri;
//    }

    @Override
    public MahasiswaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.event_cardview, parent, false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MahasiswaViewHolder holder, final int position) {
        holder.txtTitle.setText(dataList.get(position).getTitle());
        holder.txtCategory.setText(dataList.get(position).getCategory());
        holder.txtDescription.setText(dataList.get(position).getDescription());
        holder.txtLink.setText(dataList.get(position).getLink());
//        holder.txtImg.setImageURI(Uri.parse(dataList.get(position).getImg()));
        //g
        Picasso.with(holder.itemView.getContext()).load(dataList.get(position).getImg()).into(holder.txtImg);
        holder.btnShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                shareImage(dataList.get(position).getImg(), dataList.get(position).getTitle()
                        + "\n" + dataList.get(position).getLink(), v.getContext());


            }
        });
        holder.txtImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.event_dialog_lihat);
                dialog.setTitle("Poster");
                PhotoView image = (PhotoView) dialog.findViewById(R.id.image);
                Picasso.with(v.getContext()).load(dataList.get(position).getImg().replace("-150x150", "")).into(image);
                dialog.show();
            }
        });
        holder.btnLihat.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataList.get(position).getLink()));
                v.getContext().startActivity(browserIntent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class MahasiswaViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtLink, txtCategory, txtDescription;
        private ImageView txtImg;
        private Button btnLihat, btnShare;

        public MahasiswaViewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.list_title);
            txtLink = (TextView) itemView.findViewById(R.id.list_link);
            txtCategory = (TextView) itemView.findViewById(R.id.list_category);
            txtDescription = (TextView) itemView.findViewById(R.id.list_description);
            txtImg = (ImageView) itemView.findViewById(R.id.list_image);
            btnShare = (Button) itemView.findViewById(R.id.list_share);
            btnLihat = (Button) itemView.findViewById(R.id.list_lihat);
        }
    }
}