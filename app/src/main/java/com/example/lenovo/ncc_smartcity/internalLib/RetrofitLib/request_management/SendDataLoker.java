package com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management;


import com.example.lenovo.ncc_smartcity.tanggap.tanggap_model.ResponseGet;
import com.example.lenovo.ncc_smartcity.lowongan_pekerjaan.model_loker.MainResponse;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 21/03/2018.
 */

public interface SendDataLoker {

    //get for dashboard
    @Multipart
    @POST("idrekan/apiidrekan/get_data_all")
    Call<ResponseGet> getItemAll(@Part("token_m") RequestBody token_m);

    //get for dashboard
    @FormUrlEncoded
    @POST("api/idrekan/apiidrekan/get_data_idrekan")
    Call<ResponseBody> getIdrekanData(@Field("token") String token);

    @FormUrlEncoded
    @POST("api/idrekan/apiidrekan/get_data_idrekan")
    Call<MainResponse> getIdrekanDatax(@Field("token") String token);

}
