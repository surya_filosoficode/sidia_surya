package com.example.lenovo.ncc_smartcity.event;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lenovo.ncc_smartcity.event.fragment.EventMalang;
import com.example.lenovo.ncc_smartcity.event.fragment.EventNcc;
import com.example.lenovo.ncc_smartcity.pasar.fragment.pasar_fragment_bahan_bangunan;
import com.example.lenovo.ncc_smartcity.pasar.fragment.pasar_fragment_panen;
import com.example.lenovo.ncc_smartcity.pasar.fragment.pasar_fragment_sembako;

/**
 * Created by arimahardika on 11/04/2018.
 */

public class PageAdapterEvent extends FragmentPagerAdapter {

    private int numberOfTabs;

    public PageAdapterEvent(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new EventMalang();
            case 1:
                return new EventNcc();

        }
        return null;
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
