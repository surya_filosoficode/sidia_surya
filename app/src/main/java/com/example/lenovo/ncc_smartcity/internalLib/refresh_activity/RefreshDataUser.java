package com.example.lenovo.ncc_smartcity.internalLib.refresh_activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.MainActivity;
import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity.internalLib.Session;
import com.example.lenovo.ncc_smartcity.login.MainLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 14/05/2018.
 */

public class RefreshDataUser {

    String TAG = "RefreshDataUser";

    public Boolean create_session(JSONObject dataJson, Context context) {
//        Log.e(TAG, "create_session: "+dataJson.toString());
        try {
            if (dataJson != null) {
                Log.e(TAG, "create_session: " + dataJson.toString());
                //JSONObject dataMain = dataJson.getJSONObject("main_data");
                String status = dataJson.getString("status");

                String nama = null;
                String alamat = null;
                String wn = null;
                String tmp_lhr = null;
                String tgl_lhr = null;
                String jk = null;

                String id_user = null;
                String username = null;
                String url_img = null;
                String nik = null;
                String email = null;
                String tlp = null;
                String type_penduduk = null;
//                    String pass = null;

//                    Log.e(TAG, "onResponse: " + status);
                if (status != null) {
                    //main data
                    id_user = dataJson.getString("id_user");
                    username = dataJson.getString("username");
                    url_img = dataJson.getString("url_img");
                    nik = dataJson.getString("nik");
                    email = dataJson.getString("email");
                    tlp = dataJson.getString("tlp");
                    type_penduduk = dataJson.getString("type_penduduk");

                    //second data
//                        JSONObject dataSec = dataJson.getJSONObject("sec_data");

                    nama = dataJson.getString("nama");
                    alamat = dataJson.getString("alamat");
                    wn = dataJson.getString("kewarganegaraan");
                    tmp_lhr = dataJson.getString("tmp_lhr");
                    tgl_lhr = dataJson.getString("tgl_lhr");
                    jk = dataJson.getString("jk");

//                        Log.e(TAG, "onResponse: " + id_user);
//                        Log.e(TAG, "onResponse: " + username);

                    new Session().loginSession(context,
                            id_user, username, url_img, nik, email, tlp, status, type_penduduk,
                            nama, alamat, wn, tmp_lhr, tgl_lhr, jk);

                    return true;
                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "create_session: " + e.getMessage());
        }

        return false;
    }

}
