package com.example.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;

/**
 * Created by USER on 26/02/2018.
 */

public class InfoHotel extends Activity implements View.OnClickListener {

    CardView hotel, homestay, guesthouse;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_sub_hotel);

        hotel = findViewById(R.id.hotel);
        homestay = findViewById(R.id.homestay);
        guesthouse = findViewById(R.id.guesthouse);

        hotel.setOnClickListener(this);
        homestay.setOnClickListener(this);
        guesthouse.setOnClickListener(this);


    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hotel:
                i = new Intent(this, InfoHotelSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_HOTEL);
                this.startActivity(i);
                break;
            case R.id.homestay:
                i = new Intent(this, InfoHotelSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_HOMESTAY);
                this.startActivity(i);
                break;
            case R.id.guesthouse:
                i = new Intent(this, InfoHotelSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_GUESTHOUSE);
                this.startActivity(i);
                break;

        }
    }
}
