package com.example.lenovo.ncc_smartcity.register_new;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity.internalLib.register.MainRegisterData;

/**
 * Created by USER on 22/03/2018.
 */

public class MainRegister_new extends FragmentActivity {

    Button btn_mlg, btn_no_mlg;
    Intent intent;
    String TAG = "MainRegister_new";

    MainRegisterData tempMainRegisterData = new MainRegisterData();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_main);
        btn_mlg = findViewById(R.id.btnWrgMlg);
        btn_no_mlg = findViewById(R.id.btnWrgLuar);

        String[] tmpData = tempMainRegisterData.MainRegisterGet(this);

        //Log.e(TAG, "onCreate: "+tmpData[1]);
        if (tmpData != null) {
            Log.e(TAG, "onCreate: !null");
            intent = new Intent(MainRegister_new.this, MainRegisterBio_new.class);
            startActivity(intent);
            finish();
//            Log.e(TAG, "onCreate: "+tmpData[0]);
        }

        btn_mlg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNext("0");
            }
        });

        btn_no_mlg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNext("1");
            }
        });
    }

    private void setNext(String status) {
        intent = new Intent(MainRegister_new.this, MainRegisterBio_new.class);
        intent.putExtra("status_warga", status);
        startActivity(intent);
        finish();
    }
}
