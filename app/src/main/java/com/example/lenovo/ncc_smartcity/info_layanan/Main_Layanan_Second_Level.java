package com.example.lenovo.ncc_smartcity.info_layanan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity.info_layanan.Utilities.Adapter_Layanan;

/**
 * Created by arimahardika on 23/03/2018.
 */

public class Main_Layanan_Second_Level extends Activity {

    Adapter_Layanan adapterLayanan;
    RecyclerView recyclerView;
    TextView textView;
    CharSequence[] listLayanan;
    String selectedFeature;
    Typeface customFont;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layanan_second_level);

        Log.d("KESEHATAN", "onCreate: im inside main layanan second level");

        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        textView = findViewById(R.id.tv_layanan_sub_list);
        textView.setTypeface(customFont);

        recyclerView = findViewById(R.id.layanan_recycler_2nd_level);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getExtra();

        getContentBasedOnCategory(selectedFeature);
    }

    private void getContentBasedOnCategory(String selected) {
        switch (selected) {

            //<editor-fold desc="LIST FITUR KEPENDUDUKAN">
            case StaticVariable.KARTU_KELUARGA:
                listLayanan = getResources().getTextArray(R.array.list_layanan_kk);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;
            case StaticVariable.AKTA:
                listLayanan = getResources().getTextArray(R.array.list_layanan_akta);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;
            case StaticVariable.KTP:
                listLayanan = getResources().getTextArray(R.array.list_layanan_ktp);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;
            //</editor-fold>

            //<editor-fold desc="LIST FITUR RUMAH SAKIT + PUSKESMAS_UTAMA">
            case StaticVariable.RUMAH_SAKIT:
                listLayanan = getResources().getTextArray(R.array.list_rumah_sakit);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.PUSKESMAS_UTAMA:
                listLayanan = getResources().getTextArray(R.array.list_puskesmas);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.PELAYANAN_PAJAK:
                textView.setText(R.string.pelayananPajak2);
                listLayanan = getResources().getTextArray(R.array.alamat_badan_pelayanan_pajak);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.DINAS_PERIZINAN:
                textView.setText(R.string.pelayananPerizinan2);
                listLayanan = getResources().getTextArray(R.array.alamat_dinas_perizinan);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;
            //</editor-fold>
        }
    }
    private void getExtra() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
        }
    }
}
