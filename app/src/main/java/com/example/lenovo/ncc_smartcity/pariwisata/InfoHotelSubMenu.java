package com.example.lenovo.ncc_smartcity.pariwisata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;

/**
 * Created by USER on 26/02/2018.
 */

public class InfoHotelSubMenu extends Activity {

    TextView textView;
    String selectedFeature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pariwisata_layout);

        textView = findViewById(R.id.tv_pariwisata_list);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
        }
        String url = null;
        switch (selectedFeature) {
            case StaticVariable.INFO_HOTEL:
                textView.setText("Info Hotel");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/1";
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_HOMESTAY:
                textView.setText("Info Homestay");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/4" ;
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

            case StaticVariable.INFO_GUESTHOUSE:
                textView.setText("Info Guesthouse");
                url = "http://sidia.malangkota.go.id/c_wisata/getEaWisata/3";
                new DapatWisata().DapatSemuaWisata(url, InfoHotelSubMenu.this, selectedFeature);
                break;

        }
    }

}