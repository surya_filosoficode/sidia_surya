package com.example.lenovo.ncc_smartcity.info_layanan.alur_layanan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;

import java.util.LinkedHashMap;

/**
 * Created by arimahardika on 07/03/2018.
 */

public class AlurLayanan extends Activity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    LinkedHashMap<String, Integer> fileMaps;
    String selectedFeature, specificFeature;
    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layanan_imageslider_alur);
        sliderLayout = findViewById(R.id.sl_sliderLayout);

        fileMaps = new LinkedHashMap<>();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
            specificFeature = (String) bundle.get(StaticVariable.SPECIFIC_FEATURE);
            //Toast.makeText(this, selectedFeature, Toast.LENGTH_SHORT).show();
        }

        getImageBaseOnCategory(selectedFeature);

        for (String name : fileMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);

            textSliderView
                    .description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);

        countImage();
    }

    private void getImageBaseOnCategory(String category) {
        switch (category) {
            case StaticVariable.KARTU_KELUARGA:
                fileMaps.clear();
                fileMaps.put("KK Baru 1", R.drawable.alur_kk_01);
                fileMaps.put("KK Baru 2", R.drawable.alur_kk_02);
                fileMaps.put("KK Baru 3", R.drawable.alur_kk_03);
                break;
            case StaticVariable.KTP:
                fileMaps.clear();
                fileMaps.put("KK Hilang Rusak 1", R.drawable.alur_ktp_01);
                fileMaps.put("KK Hilang Rusak 2", R.drawable.alur_ktp_02);
                fileMaps.put("KK Hilang Rusak 3", R.drawable.alur_ktp_03);
                break;
            case StaticVariable.PAJAK:
                fileMaps.clear();
                fileMaps.put("Alur Pembayaran Pajak Daerah", R.drawable.alur_pembayaran_pajak_daerah);
                break;
            case StaticVariable.PERIZINAN:
                fileMaps.clear();
                fileMaps.put("Alur Pelayanan Perizinan", R.drawable.alur_pelayanan_perijinan);
                break;
            case StaticVariable.AKTA:
                Log.d("IMAGESLIDER", "getImageBaseOnCategory: " + specificFeature);

                if (specificFeature.equals(StaticVariable.SUBTITLE_AKTA_KELAHIRAN)) {
                    fileMaps.clear();
                    fileMaps.put("Akta Kelahiran 1", R.drawable.alur_akta_kelahiran_01);
                    fileMaps.put("Akta Kelahiran 2", R.drawable.alur_akta_kelahiran_02);

                } else if (specificFeature.equals(StaticVariable.SUBTITLE_AKTA_KEMATIAN)) {
                    fileMaps.clear();
                    fileMaps.put("Akta Kematian 1", R.drawable.alur_akta_kematian_01);
                    fileMaps.put("Akta Kematian 2", R.drawable.alur_akta_kematian_02);

                } else if (specificFeature.equals(StaticVariable.SUBTITLE_AKTA_PENCATATAN_PENGAKUAN_ANAK)) {
                    fileMaps.clear();
                    fileMaps.put("Pencatatan Pengakuan Anak 1", R.drawable.alur_pencatatan_pengakuan_anak_01);
                    fileMaps.put("Pencatatan Pengakuan Anak 2", R.drawable.alur_pencatatan_pengakuan_anak_02);
                    fileMaps.put("Pencatatan Pengakuan Anak 3", R.drawable.alur_pencatatan_pengakuan_anak_03);
                    fileMaps.put("Pencatatan Pengakuan Anak 4", R.drawable.alur_pencatatan_pengakuan_anak_04);
                }
                break;

        }
    }

    private void countImage() {
        if (fileMaps.size() < 2) {
            sliderLayout.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float position) {
                }
            });
        }
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
