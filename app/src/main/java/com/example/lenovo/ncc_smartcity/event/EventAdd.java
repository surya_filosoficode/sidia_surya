package com.example.lenovo.ncc_smartcity.event;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.lenovo.ncc_smartcity.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by lenovo on 2/5/2018.
 */

public class EventAdd extends AppCompatActivity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    //    ListView listView;
    private static CustomAdapterEvent adapter;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    ArrayList<DataModel> dataModels;
    ProgressDialog progressDialog;
    String loadingText = "Loading";
    Button submit, gambarGaleri;
    ImageView gambar;
    Uri tempatGambar;
    GetKategori getKategori = new GetKategori();
    private RecyclerView recyclerView;

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_add);
        ButterKnife.bind(this);
        getKategori.listSemuaKategori((LinearLayout) findViewById(R.id.rootView));
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24px));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gambarGaleri = (Button) findViewById(R.id.galeri);

        gambar = (ImageView) findViewById(R.id.preview_gambar);
        verifyStoragePermissions(this);
//        final EditText nama=(EditText) findViewById(R.id.nama);
//        final EditText deskripsi=(EditText) findViewById(R.id.description);
//        final EditText kategori=(EditText) findViewById(R.id.kategori);

        gambarGaleri.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 0);//
            }
        });

    }

    @OnClick({R.id.submit_event})
    public void selesai() {
        Log.e("lucu", Integer.toString(getKategori.spinner.getSelectedItemPosition()));
        //    final Dialog dialog = new Dialog(EventAdd.this);
//    dialog.setContentView(R.layout.event_dialog_lihat);
//    dialog.setTitle("Poster");
////Log.e("tempat",tempatGambar.toString());
//    ApiService service = RetroClient.getApiService();
////Log.e("ds",getRealPathFromUri(EventAdd.this,tempatGambar));
////                //File creating from selected URL
//
//    File file = new File(getRealPathFromUri(EventAdd.this, tempatGambar));
//
////
////                // create RequestBody instance from file
//    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
////
//    MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
////                MultipartBody.Part body =
////                        MultipartBody.Part.createFormData("lucu","sumpaon");
//    Call<Result> resultCall = service.uploadEvent(body);
//
//    resultCall.enqueue(new Callback<Result>() {
//        @Override
//        public void onResponse(Call<Result> call, Response<Result> response) {
//            Log.e("keberhasilan", response.body().getResult());
//
//
//        }
//
//        @Override
//        public void onFailure(Call<Result> call, Throwable t) {
//            Log.e("haha", t.getMessage());
//        }
//    });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    tempatGambar = imageReturnedIntent.getData();
                    Log.e("tempat", tempatGambar.toString());
                    Uri selectedImage = imageReturnedIntent.getData();
                    gambar.setImageURI(selectedImage);
                    gambar.requestLayout();
                    gambar.getLayoutParams().height = 400;
                }

                break;

        }
    }

    //hai
    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

}
