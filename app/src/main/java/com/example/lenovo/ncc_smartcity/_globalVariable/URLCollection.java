package com.example.lenovo.ncc_smartcity._globalVariable;

/**
 * Created by arimahardika on 07/03/2018.
 */

public class URLCollection {

    //region Data source kesehatan

    public static final String DATA_SOURCE_KESEHATAN_RUMAH_SAKIT_UMUM = "https://api.myjson.com/bins/1b57g1";

    public static final String DATA_SOURCE_KESEHATAN_RUMAH_SAKIT = "https://sampankecil.scalingo.io/rumah_sakit.json";

    public static final String DATA_SOURCE_KESEHATAN_RUMAH_SAKIT_IBU_ANAK = "https://api.myjson.com/bins/j245b";

    public static final String DATA_SOURCE_KESEHATAN_RUMAH_SAKIT_BERSALIN = "https://api.myjson.com/bins/10boov";

    public static final String DATA_SOURCE_KESEHATAN_PUSKESMAS = "https://sampankecil.scalingo.io/puskesmas.json";

    public static final String DATA_SOURCE_KESEHATAN_PUSKESMAS_UPT_PEMBANTU = "https://api.myjson.com/bins/5yku7";

    public static final String DATA_SOURCE_KESEHATAN_KLINIK = "https://api.myjson.com/bins/1hky7i";

    public static final String DATA_SOURCE_KESEHATAN_APOTEK = "https://api.myjson.com/bins/nswg7";

    public static final String DATA_SOURCE_KESEHATAN_DOKTER_GIGI = "https://api.myjson.com/bins/rbtqn";

    public static final String DATA_SOURCE_KESEHATAN_LABORATORIUM = "https://api.myjson.com/bins/1gxgr3";

    public static final String DATA_SOURCE_KESEHATAN_OPTIK = "https://api.myjson.com/bins/lyxan";

    //endregion ====================================================================================

    public static final String DATA_SOURCE_EVENT = "https://sampankecil.scalingo.io/eventmalang";

    public static final String DATA_SOURCE_PASAR = "http://ncc.malangkota.go.id/smartcity/hargapasar";

    public static final String DATA_SOURCE_JADWAL_SHOLAT_PKPU = "http://ncc.malangkota.go.id/smartcity/jadwalshalat";

    public static final String DATA_SOURCE_PERKIRAAN_CUACA = "http://sampankecil.scalingo.io/cuaca";

    public static final String DATA_SOURCE_OPEN_WEATHER = "http://api.openweathermap.org/data/2.5/weather?id=1636722&APPID=3abdb79dc61483be3832d781814a1928&units=metric";

    public static final String DATA_SOURCE_JADWAL_SHOLAT = "http://muslimsalat.com/malang.json?key=bd099c5825cbedb9aa934e255a81a5fc";

    public static final String DATA_SOURCE_TANGGAP = "https://api.myjson.com/bins/mlygd";

//    public static final String DATA_SOURCE_LOKER = "http://192.168.86.79:8080/sidia2/api/";
    public static final String DATA_SOURCE_LOKER = "http://sidia.malangkota.go.id/api/";
//    public static final String DATA_SOURCE_LOKER = "http://192.168.43.130:8080/ncc_admin/";

    public static final String DATA_SOURCE_LOGIN = "http://192.168.86.57:8080/sidia/";

    //region Data source berita

    public static final String DATA_SOURCE_BERITA_HOST = "www.malangkota.go.id";

    public static final String DATA_SOURCE_BERITA_UTAMA = "https://mediacenter.malangkota.go.id/category/utama/";

    public static final String DATA_SOURCE_BERITA_FEATURES = "https://mediacenter.malangkota.go.id/category/features/";

    public static final String DATA_SOURCE_BERITA_EKONOMI_BISNIS = "https://mediacenter.malangkota.go.id/category/berita/ekonomi-bisnis/";

    public static final String DATA_SOURCE_BERITA_POLITIK = "https://mediacenter.malangkota.go.id/category/berita/pemerintahan-politik/";

    public static final String DATA_SOURCE_BERITA_LAYANAN_PUBLIK = "https://mediacenter.malangkota.go.id/category/berita/layanan-publik-berita/";

    public static final String DATA_SOURCE_BERITA_OLAHRAGA = "https://mediacenter.malangkota.go.id/category/berita/olah-raga-berita/";

    public static final String DATA_SOURCE_BERITA_PEMBANGUNAN_LINGKUNGAN = "https://mediacenter.malangkota.go.id/category/berita/pembangunan-lingkungan/";

    public static final String DATA_SOURCE_BERITA_PENDIDIKAN = "https://mediacenter.malangkota.go.id/category/berita/pendidikan-berita/";

    public static final String DATA_SOURCE_BERITA_KESRA = "https://mediacenter.malangkota.go.id/category/berita/kesra/";

    public static final String DATA_SOURCE_BERITA_HIBURAN = "https://mediacenter.malangkota.go.id/category/hiburan/";

    //endregion

    //Url Retrofit Api Tanggap local

//    public static final String BASE_URL_TANGGAP = "http://192.168.86.79:8080/sidia2/api/";
//    public static final String BASE_URL_FOTO = "http://192.168.86.79:8080/sidia2/";


    public static final String BASE_URL_TANGGAP = "http://sidia.malangkota.go.id/api/";
    public static final String BASE_URL_FOTO = "http://sidia.malangkota.go.id/";

    public static final String BASE_URL_LOCAL = "http://192.168.86.57:8080/sidia2/api/";

    public static final String BASE_URL_IDREKAN = "http://192.168.86.57:8080/sidia2/";

}
