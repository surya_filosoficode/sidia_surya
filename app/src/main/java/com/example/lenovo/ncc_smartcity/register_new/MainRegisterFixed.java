package com.example.lenovo.ncc_smartcity.register_new;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.example.lenovo.ncc_smartcity.internalLib.register.MainRegisterData;
import com.example.lenovo.ncc_smartcity.internalLib.register.SecondRegisterData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 04/04/2018.
 */

public class MainRegisterFixed extends FragmentActivity {
    MessageVariabe messageVariabe = new MessageVariabe();
    String TAG = "MainRegisterFixed";
    String TOKEN_MOBILE = "X00W";
    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    ProgressDialog progressDialog;
    Intent intent;
    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;


    CheckConn checkConn = new CheckConn();

    TextView nik, username, email, tlp, type_penduduk, nama, alamat, tgl_lhr, tmp_lhr, jk, wn, pass;
    String nik_ = "", username_ = "", email_ = "", tlp_ = "", pass_ = "",
            nama_ = "", alamat_ = "", tgl_lhr_ = "", tmp_lhr_ = "", jk_ = "", wn_ = "";
    Button btnNext, btnPrev;

    String val_type_penduduk = "";

    String[] tmp_data_main, tmp_data_second;

    MainRegisterData tmpMainRegisterData = new MainRegisterData();
    SecondRegisterData tmpSecondRegisterData = new SecondRegisterData();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_fixed_new);

        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrev = (Button) findViewById(R.id.btnPrev);

        nik = (TextView) findViewById(R.id.val_nik);
        username = (TextView) findViewById(R.id.val_username);
        email = (TextView) findViewById(R.id.val_email);
        tlp = (TextView) findViewById(R.id.val_tlp);
        type_penduduk = (TextView) findViewById(R.id.val_ty_warga);
        nama = (TextView) findViewById(R.id.val_nama);
        alamat = (TextView) findViewById(R.id.val_alamat);
        tgl_lhr = (TextView) findViewById(R.id.val_tgl_lhr);
        tmp_lhr = (TextView) findViewById(R.id.val_tmp_lhr);
        jk = (TextView) findViewById(R.id.val_jk);
        wn = (TextView) findViewById(R.id.val_wn);
        pass = (TextView) findViewById(R.id.val_pass);

        tmp_data_main = tmpMainRegisterData.MainRegisterGet(MainRegisterFixed.this);
        tmp_data_second = tmpSecondRegisterData.SecondRegisterGet(MainRegisterFixed.this);

        if (tmp_data_main != null) {
            nik_ = tmp_data_main[0];
            username_ = tmp_data_main[1];
            email_ = tmp_data_main[2];
            tlp_ = tmp_data_main[3];
            val_type_penduduk = tmp_data_main[4];
            pass_ = tmp_data_main[5];

            nik.setText(nik_);
            username.setText(username_);
            email.setText(email_);
            tlp.setText(tlp_);
            type_penduduk.setText(val_type_penduduk);
            pass.setText(pass_);
        }

        if (tmp_data_second != null) {

            nama_ = tmp_data_second[0];
            alamat_ = tmp_data_second[1];
            tgl_lhr_ = tmp_data_second[3];
            tmp_lhr_ = tmp_data_second[2];
            jk_ = tmp_data_second[4];
            wn_ = tmp_data_second[5];

            nama.setText(nama_);
            alamat.setText(alamat_);
            tgl_lhr.setText(tgl_lhr_);
            tmp_lhr.setText(tmp_lhr_);
            jk.setText(jk_);
            wn.setText(wn_);
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkConn.isConnected(MainRegisterFixed.this)) {
                    progressDialog = new ProgressDialog(MainRegisterFixed.this) {
                        @Override
                        public void onBackPressed() {
                            finish();
                        }
                    };

                    progressDialog.setMessage("mohon tunggu ..!");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    base_url_management = Base_url.getClient_notGson(BASE_URL)
                            .create(SendDataLogin.class);

                    if (Integer.parseInt(val_type_penduduk) == 0) {
                        getdata = base_url_management.RegisterRequestMlg(nik_, username_,
                                pass_, email_, tlp_, val_type_penduduk, TOKEN_MOBILE);

                    } else {
                        getdata = base_url_management.RegisterRequestNoMlg(nik_, username_,
                                pass_, email_, tlp_, val_type_penduduk,
                                nama_, jk_, alamat_, wn_, tmp_lhr_, tgl_lhr_,
                                TOKEN_MOBILE);

                    }

                    adapterRetrofit(getdata);
                } else {
                    Toast.makeText(MainRegisterFixed.this, messageVariabe.MESSAGE_ERROR_DISCONNECTED,
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(val_type_penduduk) == 0) {
                    intent = new Intent(MainRegisterFixed.this, MainRegisterBio_new.class);
                } else {
                    intent = new Intent(MainRegisterFixed.this, MainRegisterBioDetail_new.class);
                }
                startActivity(intent);
                finish();
            }
        });
    }

    public void adapterRetrofit(Call<ResponseBody> getdata) {

        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
//                try {
//                    Log.e(TAG, "onResponse: "+response.body().string());
//                } catch (IOException e) {
//                    Log.e(TAG, "onResponse: "+e.getMessage());
//                }
                try {
                    try {
                        dataJson = new JSONObject(response.body().string());
                        Log.e(TAG, "onResponse: " + dataJson);
                        boolean status = dataJson.getJSONObject("body_message").getBoolean("status");
                        message_out = dataJson.getJSONObject("body_message").getString("message");
                        if (status) {
                            tmpMainRegisterData.MainRegisterDestroy(MainRegisterFixed.this);
                            tmpSecondRegisterData.SecondRegisterDestroy(MainRegisterFixed.this);

                            intent = new Intent(MainRegisterFixed.this, TakePhoto_new.class);
                            intent.putExtra("nik", nik_);
                            intent.putExtra("ty_take_img", "0");
                            intent.putExtra("action", "0");
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(MainRegisterFixed.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER,
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        Toast.makeText(MainRegisterFixed.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID,
                                Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onResponse: " + e);
                    }
                    Toast.makeText(MainRegisterFixed.this, message_out, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Toast.makeText(MainRegisterFixed.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID,
                            Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onResponse: dataJson: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainRegisterFixed.this, messageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER,
                        Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t);
            }
        });

    }
}
