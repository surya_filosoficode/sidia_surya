package com.example.lenovo.ncc_smartcity.tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.lenovo.ncc_smartcity.R;

import java.util.LinkedHashMap;

public class Tutorial extends Activity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    LinkedHashMap<String, Integer> fileMaps;
    String selectedFeature;
    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        sliderLayout = findViewById(R.id.sl_tutorialSliderLayout);

        fileMaps = new LinkedHashMap<>();

        populateFileMaps();

        for (String name : fileMaps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView.description(name)
                    .image(fileMaps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
        sliderLayout.addOnPageChangeListener(this);
    }

    private void populateFileMaps() {
        fileMaps.clear();
        fileMaps.put("Halaman Utama", R.drawable.t_halamanutama1);
        fileMaps.put("Halaman Registrasi", R.drawable.t_registrasi2);
        fileMaps.put("Halaman Login", R.drawable.t_login3);
        fileMaps.put("Info Layanan", R.drawable.t_infolayanan4);
        fileMaps.put("Berita", R.drawable.t_infoberita5);
        fileMaps.put("Pariwisata", R.drawable.t_infopariwisata6);
        fileMaps.put("Event", R.drawable.t_event7);
        fileMaps.put("Telepon Darurat", R.drawable.t_telpdarurat8);
        fileMaps.put("Harga Pasar", R.drawable.t_hargapasar9);
        fileMaps.put("Idrekan", R.drawable.t_idrekan10);
    }

    @Override
    protected void onStop() {
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
