package com.example.lenovo.ncc_smartcity.tanggap;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.MainActivity;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.CheckGps;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTanggap;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.squareup.picasso.Picasso;

import com.example.lenovo.ncc_smartcity.internalLib.Session;
import com.example.lenovo.ncc_smartcity.internalLib.SessionCheck;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 15/03/2018.
 */

public class PostData extends AppCompatActivity {

    static final int REQUEST_LOCATION = 1;
    static final int READ_EXTERNAL = 2;
    static final int WRITE_EXTERNAL = 3;
    static final int USE_API_23_AND_ABOVE = 23;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    //region Initialize Variable
    String TOKEN_MOBILE = "X00W";
    //variable send
    String id_post, id_user, id_type, desc, foto_data,
            type_warga, status_vert;
    //location variable
    String loc_prov;
    SendDataTanggap api;
    //geo coder
    Geocoder geocoder;
    EditText edtDeskripsi;
    Spinner spKategori;
    ImageButton foto;
    ImageView takePhoto;
    Button btnBatal, btnKirim;
    String user, lat, lng, address;
    ProgressDialog pd;
    LocationManager locationManager;
    File mFileUri;
    ByteArrayOutputStream outputStream;
    String BASE_URL = URLCollection.BASE_URL_TANGGAP;
    String TAG = "PostActivity";
    String currentPhotoPath;
    String[] data_session, loc;

    Intent intent;

    Bitmap originalImage;
    int width;
    int height;
    int newWidth = 900;
    int newHeight = 720;
    Matrix matrix;
    Bitmap resizedBitmap;
    float scaleWidth;
    float scaleHeight;
    String imagePath = "";

    String add_send = null;

    CheckConn checkConn = new CheckConn();
    CheckGps checkGps = new CheckGps();

    private GoogleApiClient googleApiClient;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    @Nullable
    private File getMediaFileName() {
        // Lokasi External sdcard
        if (ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 110);
        }


        File mediaStorageDir = new
                File(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))+File.separator+"Tanggap");
        Log.e(TAG, "onCreate: "+ ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE));
        Log.e(TAG, "onCreate: "+ PackageManager.PERMISSION_GRANTED);

        Log.e("createFile", "getMediaFileName: "+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES ).toString());
        Log.e("createFile", "getMediaFileName: "+Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES )+"/tanggap/".toString());



        Log.e("createFile", "mediadir: "+ mediaStorageDir.exists());
        Log.e("createFile", "mediadir: "+ mediaStorageDir);
        Log.e("createFile", "mkdir: "+ mediaStorageDir.mkdir());

//        try{
////            file.mkdir();
////            file.mkdirs();
//            Log.e(TAG, "getMediaFileName: mantap");
//        }catch (Exception e){
//            Log.e(TAG, "getMediaFileName: "+e);
//        }


        if (!mediaStorageDir.exists()) {

//errors
            try{

                mediaStorageDir.mkdirs();
                Log.e(TAG, "getMediaFileName: mantap");
            }catch (Exception e){
                Log.e(TAG, "getMediaFileName: "+e);
            }
            if (!mediaStorageDir.mkdir()) {
                Log.e("createFile", "Gagal Membuat Directory" + "CameraDemo");
                return null;
            }else {
                Log.e("createFile", "getMediaFileName: berhasil membuat file" );
            }
        }

        File mediaFile = null;
        // Membuat nama file
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "TGP_IMG_" + timeStamp
                + ".jpg");
        Log.e("createFile", "getMediaFileName: " + mediaFile);
        return mediaFile;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tanggap_post_data);

        googleApiClient = getAPIClientInstance();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }

        requestGPSSettings();

        if (ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALENDAR}, 13);

        }

        Log.e(TAG, "onCreate: write"+ ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE));
        Log.e(TAG, "onCreate: "+ PackageManager.PERMISSION_GRANTED);

        //create progress dialog
        pd = new ProgressDialog(this) {
            public void onBackPressed() {
                finish();
            }
        };

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        newHeight = displayMetrics.widthPixels;
        newWidth = displayMetrics.heightPixels;

        if (Build.VERSION.SDK_INT > 19) {
            newHeight = displayMetrics.heightPixels;
            newWidth = displayMetrics.widthPixels;
        }

        CheckPermissionLocation(PostData.this);

        edtDeskripsi = findViewById(R.id.et_description);
        spKategori = findViewById(R.id.spinner_kategori);
        btnBatal = findViewById(R.id.btn_cancel);
        btnKirim = findViewById(R.id.btn_send);
        takePhoto = findViewById(R.id.iv_takePhoto);

        takePhoto.getLayoutParams().height = displayMetrics.widthPixels;
        takePhoto.getLayoutParams().height = displayMetrics.heightPixels;


        //strict mode for oreo
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //check session
        data_session = new SessionCheck().seesionLoginChecker(this);
        id_user = null;



        btnKirim.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                id_type = String.valueOf(spKategori.getSelectedItemId() + 1);
                desc = edtDeskripsi.getText().toString();

                if (new CheckGps().check_gps(PostData.this) ||
                        new CheckConn().isConnected(PostData.this)) {

                    Log.e(TAG, "onClick: CheckGps: true");

//                    Double[] loc_now = LocationNow();
//                    loc = getCompleteAddressString(loc_now);
                    MultipartBody.Part body = null;
                    String PathPhoto = imagePath;
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        PathPhoto = currentPhotoPath;
//                    } else {
//                        PathPhoto = imagePath;
//                    }

                    if (!PathPhoto.isEmpty() && id_user != null) {
                        File file = new File(PathPhoto);
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        body = MultipartBody.Part.createFormData("gambar", file.getName(), requestFile);
                        RequestBody id_user_r = MultipartBody.create(MediaType.parse("multipart/form-data"), id_user);
                        RequestBody id_type_r = MultipartBody.create(MediaType.parse("multipart/form-data"), id_type);
                        RequestBody loc_r = MultipartBody.create(MediaType.parse("multipart/form-data"), add_send);
                        RequestBody desc_r = MultipartBody.create(MediaType.parse("multipart/form-data"), desc);
                        RequestBody token_r = MultipartBody.create(MediaType.parse("multipart/form-data"), TOKEN_MOBILE);

                        if (body != null && add_send != null && desc != null) {
                            api = Base_url.getClient(BASE_URL).create(SendDataTanggap.class);
                            Call<ResponseBody> getdata = api.insertData(id_user_r, id_type_r, loc_r, desc_r, body, token_r);

                            getdata.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                                    try {
//                                        Log.e(TAG, "onResponse: "+response.body().string());
//                                    } catch (IOException e) {
//                                        Log.e(TAG, "onResponse: "+e.toString());
//                                    }

                                    Log.e(TAG, "onResponse: run");
                                    try {
                                        JSONObject data_response = new JSONObject(response.body().string());
                                        Log.e(TAG, "onResponse: run: " + data_response);
                                        boolean status = Boolean.valueOf(data_response.getJSONObject("body_message").getString("status"));
                                        if (status) {
//                                                intent = new Intent(PostData.this, Main_tanggap.class);
//                                                startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(PostData.this, "Gagal Memasukkan Silahkan Anda Coba Lagi", Toast.LENGTH_LONG).show();
                                        }

                                    } catch (JSONException e) {
                                        Log.e(TAG, "onResponse: " + e.getMessage());
                                        Toast.makeText(PostData.this, "Gagal Memasukkan Silahkan Anda Coba Lagi", Toast.LENGTH_LONG).show();
                                    } catch (IOException e) {
                                        Log.e(TAG, "onResponse: " + e.getMessage());
                                        Toast.makeText(PostData.this, "Gagal Memasukkan Silahkan Anda Coba Lagi", Toast.LENGTH_LONG).show();
                                    }


                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Log.e(TAG, "onFailure: " + t);
                                }
                            });

                        } else {
                            Log.e(TAG, "onClick: img null && loc && desc");
                            Toast.makeText(PostData.this, "Masukkan Dengan Benar", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Log.e(TAG, "onClick: Masukkan Dengan Benar");
                        Toast.makeText(PostData.this, "Masukkan Dengan Benar", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e(TAG, "onClick: Masukkan Dengan Benar");
                    Toast.makeText(PostData.this, "Untuk Memposting Dalam Menu Ini Pastikan GPS dan jaringan anda Telah Aktif..", Toast.LENGTH_SHORT).show();
                }
                //endregion
            }
        });

        btnBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (takePhoto == null && edtDeskripsi == null) {
                    Intent in = new Intent(PostData.this, Main_tanggap.class);
                    startActivity(in);
                } else if (takePhoto != null && edtDeskripsi != null) {
                    takePhoto = null;
                    edtDeskripsi = null;
                }
            }
        });


        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (checkConn.isConnected(PostData.this)
//                        || checkGps.check_gps(PostData.this)) {
//                    Double[] locat = LocationNow();
//                    String[] add = getCompleteAddressString(locat);
//
//                    try{
//                        if ((add[1].toLowerCase().equals("kota malang") && add[2].toLowerCase().equals("jawa timur"))) {
//                            add_send = add[0];
//                            Log.e(TAG, "onClick: " + add[0]);
//                            Log.e(TAG, "onClick: " + add[1]);
//                            Log.e(TAG, "onClick: " + add[2]);
                getMediaFileName();
                            dispatchTakePictureIntent();
//                        }else{
//                            Log.e(TAG, "onClick: gambar bukan di malang");
//                            //gambar bukan di malang
//                        }
//                    }catch (Exception e){
//                        Log.e(TAG, "onClick: oops salah");
//                    }
//                }else {
//                    Toast.makeText(PostData.this,"tolong",Toast.LENGTH_LONG).show();
//                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        //requestGPSSettings();
    }

    private void dispatchTakePictureIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            if (ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR}, 13);

            }
            mFileUri = getMediaFileName();

            //Uri outputfile = Uri.fromFile(mFileUri);
            //CalendarContract.CalendarCache.URI.fromFile(mFileUri)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, CalendarContract.CalendarCache.URI.fromFile(mFileUri));
            //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputfile);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String filePath = "";
        Log.e(TAG, "onActivityResult: mFileUri:" + requestCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                try{
                    Log.e(TAG, "onActivityResult: run");
                    filePath = mFileUri.getPath();
                    Log.e(TAG, "onActivityResult: mFileUri:" + filePath);

                }catch (Exception e){
                    Log.e(TAG, "onActivityResult: error file path:"+e.getMessage());
                }
            }

            try {
                Bitmap bitmapLoaded = BitmapFactory.decodeFile(filePath);
                Log.e(TAG, "onActivityResult: mFileUri: " + mFileUri.getPath());
                int width = bitmapLoaded.getWidth();
                int height = bitmapLoaded.getHeight();

                Log.e(TAG, "onActivityResult: " + width);
                Log.e(TAG, "onActivityResult: " + height);

                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                Matrix bitmapMatrix = new Matrix();
                bitmapMatrix.postScale(scaleWidth, scaleHeight);

                if (Build.VERSION.SDK_INT < 21) {
                    bitmapMatrix.postRotate(90);
                }

                Bitmap resizedBitmap = Bitmap.createBitmap(bitmapLoaded, 0, 0, width, height, bitmapMatrix, true);

                FileOutputStream out = null;
                String filename = mFileUri.getPath();

                try {
                    if (ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_CALENDAR}, 13);

                    }
                    //errors
                    out = new FileOutputStream(filename);
                    Log.e(TAG, "onActivityResult: FileOutputStream: " + out);
                } catch (FileNotFoundException e) {
                    Log.e(TAG, "onActivityResult: FileNotFoundException: " + e.toString());
                }

                Log.e(TAG, "onActivityResult: filename: " + filename);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
                //takePhoto.setImageBitmap(resizedBitmap);

                // resize image
                Picasso.with(getApplicationContext()).load(new File(mFileUri.getPath())).into(takePhoto);

                imagePath = mFileUri.getPath();
                Log.e(TAG, "onActivityResult: " + imagePath);

            } catch (Exception e) {
                Log.e("PHOTOPATH", "LOAD IMAGE FAILED - " + e.toString());
                Toast.makeText(this, "LOAD IMAGE FAILED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //region Request Permission

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_LOCATION:
                break;
            case READ_EXTERNAL:
                break;
            case WRITE_EXTERNAL:
                break;
        }
    }

    public void CheckPermissionLocation(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e(TAG, "onMapReady: fail");
            requestPermissions();
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
    }
    //endregion

    public Double[] LocationNow() {
        if (ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(PostData.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "onMapReady: fail");
            requestPermissions();
        }

        Double lat = 0.0,
                lng = 0.0;

        locationManager = (LocationManager) PostData.this.getSystemService(Context.LOCATION_SERVICE);
        loc_prov = LocationManager.GPS_PROVIDER;

        Location lastKnowLocation = locationManager.getLastKnownLocation(loc_prov);
        Double[] location_get = new Double[]{null, null};

        try {
            Log.e(TAG, "LocationNow: " + lastKnowLocation.getLatitude());
            Log.e(TAG, "LocationNow: " + lastKnowLocation.getLongitude());
            lat = lastKnowLocation.getLatitude();
            lng = lastKnowLocation.getLongitude();
        } catch (Exception e) {
            Log.e(TAG, "LocationNow: " + e);
        }

        location_get = new Double[]{lat, lng};
        return location_get;

    }

    public String[] getCompleteAddressString(Double[] loc) {
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        String address = null, city = null, province = null;
        if (loc[0] != 0.0 && loc[1] != 0.0) {
            try {
                addresses = geocoder.getFromLocation(loc[0], loc[1], 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city = addresses.get(0).getSubAdminArea();
                province = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//            String knownName = addresses.get(0).getFeatureName(); // Only
                Log.e(TAG, "getCompleteAddressString: " + address);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "getCompleteAddressString: " + e);
            }
        }
        return new String[]{address, city, province};
    }

    private GoogleApiClient getAPIClientInstance() {
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        return mGoogleApiClient;
    }

    private void requestGPSSettings() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(2000);
        locationRequest.setFastestInterval(500);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e(TAG, "All location settings are satisfied.");
                        Toast.makeText(getApplication(), "GPS is already enable", Toast.LENGTH_SHORT).show();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.e(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");
                        try {
                            status.startResolutionForResult(PostData.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e("Applicationsett", e.toString());
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created.");
                        Toast.makeText(getApplication(), "Location settings are inadequate, and cannot be fixed here", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }



}