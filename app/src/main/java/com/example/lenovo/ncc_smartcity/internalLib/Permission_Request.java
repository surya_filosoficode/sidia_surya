package com.example.lenovo.ncc_smartcity.internalLib;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Created by USER on 15/03/2018.
 */

public class Permission_Request {

    String TAG = "Permission_Request";

    public void CheckPermissionLocation(Context context, Activity activity,
                                        String permission, int requestCode) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            Log.e(TAG, "onMapReady: fail");
            requestPermissions(activity, permission, requestCode);
        }
    }

    private void requestPermissions(Activity activity, String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{permission}, requestCode);
    }


}
