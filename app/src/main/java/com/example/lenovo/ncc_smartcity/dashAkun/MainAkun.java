package com.example.lenovo.ncc_smartcity.dashAkun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.MainActivity;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.MessageVariabe;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.CheckConn;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.example.lenovo.ncc_smartcity.internalLib.Session;
import com.example.lenovo.ncc_smartcity.internalLib.SessionCheck;
import com.example.lenovo.ncc_smartcity.internalLib.TextSafety;
import com.example.lenovo.ncc_smartcity.internalLib.refresh_activity.RefreshDataUser;
import com.example.lenovo.ncc_smartcity.login.MainLogin;
import com.example.lenovo.ncc_smartcity.register_new.TakePhoto_new;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 05/04/2018.
 */

public class MainAkun extends FragmentActivity {

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";


    TextView nik, username, email, tlp, type_penduduk, nama, alamat, tgl_lhr, tmp_lhr, jk, wn, pass;
    TextView changePhoto, logOut, namaUser, verifikasi;
    LinearLayout ll_editAll;
    Button btnEdit, btnChaPhoto, btnLogout, btnChPass;
    ImageView btnSave, btnCancle, btnRefresh;
    CircleImageView img;
    SwipeRefreshLayout swpRefresh;

    String nik_ = "", username_ = "", email_ = "", tlp_ = "", status_ = "", url_img_ = "", pass_ = "",
            nama_ = "", alamat_ = "", tgl_lhr_ = "", tmp_lhr_ = "", jk_ = "", wn_ = "";

    String val_type, val_status, val_jk, val_wn;

    String TAG = "MainAkun";
    String val_type_penduduk = "";
    String user_id = "";

    ProgressDialog progressDialog;
    Intent intent;
    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;
    //safety text
    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();
    Session session = new Session();

    int status_verifikasi = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dash_main_new);

        btnEdit = findViewById(R.id.btnEdit);

//        changePhoto = findViewById(R.id.tv_changePhoto);
//        logOut = findViewById(R.id.tv_logout);
        ll_editAll = findViewById(R.id.ll_editAll);
        namaUser = findViewById(R.id.tv_name);

//        btnChPass = (Button) findViewById(R.id.btnChPass);
        nik = (TextView) findViewById(R.id.val_nik);
        type_penduduk = (TextView) findViewById(R.id.val_ty_warga);
        nama = (TextView) findViewById(R.id.val_nama);
        alamat = (TextView) findViewById(R.id.val_alamat);
        tgl_lhr = (TextView) findViewById(R.id.val_tgl_lhr);
        tmp_lhr = (TextView) findViewById(R.id.val_tmp_lhr);
        jk = (TextView) findViewById(R.id.val_jk);
        wn = (TextView) findViewById(R.id.val_wn);
        pass = (TextView) findViewById(R.id.val_pass);
        email = (TextView) findViewById(R.id.val_email);

        username = (EditText) findViewById(R.id.etUserName);
        tlp = (EditText) findViewById(R.id.etTlp);

        img = (CircleImageView) findViewById(R.id.img_profil);

        btnSave = (ImageView) findViewById(R.id.btnSave);
        btnCancle = (ImageView) findViewById(R.id.btnCanle);
        btnRefresh = (ImageView) findViewById(R.id.btnRefresh);

//        verifikasi = (TextView) findViewById(R.id.tv_verifikasi);
        int status_call = 0;

        get_data();

//        swpRefresh = (SwipeRefreshLayout)findViewById(R.id.swpRefresh);
//        swpRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                Log.e(TAG, "onRefresh: refresh");
//                base_url_management = Base_url.getClient_notGson(BASE_URL)
//                        .create(SendDataLogin.class);
//                getdata = base_url_management.RefreshDash(user_id ,TOKEN_MOBILE);
//                setData(getdata,1);
//
//            }
//
//        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onRefresh: refresh");
                sendData();
                base_url_management = Base_url.getClient_notGson(BASE_URL)
                        .create(SendDataLogin.class);
                getdata = base_url_management.RefreshDash(user_id, TOKEN_MOBILE);
                setData(getdata, 1);
            }
        });


        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                intent = new Intent(MainAkun.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });
//        verifikasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                base_url_management = Base_url.getClient_notGson(BASE_URL)
//                        .create(SendDataLogin.class);
//
//                //0=clear, 1=email for wrg malang, 2=email & ktp for wrg non mlg, 3=ktp for wrg non mlg
//                switch (status_verifikasi){
//                    case 0:
//                        Log.e(TAG, "onClick: akun anda sudah terverifikasi");
//                        break;
//
//                    case 1:
//                        sendData();
//                        getdata = base_url_management.VerifikasiEmail(user_id
//                                ,TOKEN_MOBILE);
//                        setData(getdata,0);
//                        break;
//
//                    case 2:
//                        sendData();
//                        getdata = base_url_management.VerifikasiEmail(user_id
//                                ,TOKEN_MOBILE);
//                        setData(getdata,0);
//                        break;
//
//                    case 3:
//                        intent = new Intent(MainAkun.this,VerifikasiKTP.class);
//                            intent.putExtra("user_id",user_id);
//                        startActivity(intent);
//                        finish();
//                        break;
//
//                }
//
//
//            }
//        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
                base_url_management = Base_url.getClient_notGson(BASE_URL)
                        .create(SendDataLogin.class);

                Log.e(TAG, "onClick: " + user_id);
                getdata = base_url_management.UpdateDataAll(user_id
                        , username.getText().toString()
                        , tlp.getText().toString()
                        , TOKEN_MOBILE);
                setData(getdata, 2);
            }
        });


        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

//        changePhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                intent = new Intent(MainAkun.this, TakePhoto_new.class);
//                intent.putExtra("nik", nik_);
//                intent.putExtra("ty_take_img", "1");
//                startActivity(intent);
//                finish();
//
//            }
//        });
//
//        logOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                session.sessionDestroy(MainAkun.this);
//                intent = new Intent(MainAkun.this, MainActivity.class);
//                startActivity(intent);
//                finish();
//            }
//        });

//        btnChPass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                intent = new Intent(MainAkun.this, ChPass.class);
//                intent.putExtra("user_id", user_id);
//                startActivity(intent);
//                finish();
//            }
//        });

//        email.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                intent = new Intent(MainAkun.this, ChEmail.class);
//                intent.putExtra("id_user",user_id);
//                startActivity(intent);
//                finish();
//            }
//        });
    }

    public void get_data() {
        String[] data_session = new SessionCheck().seesionLoginChecker(MainAkun.this);
        Log.e(TAG, "onClick: " + data_session[1]);
        if (data_session != null) {


            user_id = data_session[1];
            username_ = data_session[2];
            url_img_ = data_session[3];
            nik_ = data_session[4];
            email_ = data_session[5];
            tlp_ = data_session[6];
            status_ = data_session[7];
            val_type_penduduk = data_session[8];

            nama_ = data_session[9];
            alamat_ = data_session[10];
            wn_ = data_session[11];
            tmp_lhr_ = data_session[12];
            tgl_lhr_ = data_session[13];
            jk_ = data_session[14];


//            if(Integer.parseInt(val_type_penduduk)== 0){
//                switch (Integer.parseInt(status_)){
//                    case 0:
//                        verifikasi.setText("verifikasi email");
//                        status_verifikasi = 1;
//                        break;
//                    case 1:
//                        verifikasi.setText("sudah ter-verifikasi");
//                        status_verifikasi = 0;
//                        break;
//                }
//            }else {
//                switch (Integer.parseInt(status_)){
//                    case 0:
//                        verifikasi.setText("verifikasi email dan ktp");
//                        status_verifikasi = 2;
//                        break;
//                    case 1:
//                        verifikasi.setText("sudah ter-verifikasi");
//                        status_verifikasi = 0;
//                        break;
//                    case 2:
//                        verifikasi.setText("verifikasi TKP");
//                        status_verifikasi = 3;
//                        break;
//                }
//            }


            if (Integer.parseInt(jk_) == 0) {
                val_jk = "Laki-Laki";
            } else {
                val_jk = "Perempuan";
            }

            if (Integer.parseInt(wn_) == 0) {
                val_wn = "WNI";
            } else {
                val_wn = "WNA";
            }

            if (Integer.parseInt(val_type_penduduk) == 0) {
                val_type = "Penduduk Malang";
            } else {
                val_type = "Bukan Penduduk Malang";
            }
//            nama.setText(nama_);
            alamat.setText(alamat_);
            tgl_lhr.setText(tgl_lhr_);
            tmp_lhr.setText(tmp_lhr_);
            jk.setText(val_jk);
            wn.setText(val_wn);
            namaUser.setText(nama_);
            nik.setText(nik_);
            username.setText(username_);
            tlp.setText(tlp_);
            email.setText(email_);
            type_penduduk.setText(val_type);

            if (!url_img_.isEmpty()) {
                Log.e(TAG, "get_data: url_img_: " + url_img_);
                try {
//                    Picasso.with(activity).load(mayorShipImageLink).transform(new CircleTransform()).into(ImageView);
                    Log.e(TAG, "get_data: " + BASE_URL + url_img_);
                    Picasso.with(MainAkun.this)
                            .load(new URLCollection().BASE_URL_FOTO + url_img_)
                            .error(R.drawable.icon_razer_50)
                            .into(img);
                } catch (Exception e) {
                    Log.e(TAG, "get_data: img error");
                }

            }

        }
    }


    private void sendData() {
        progressDialog = new ProgressDialog(MainAkun.this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(MessageVariabe.MESSAGE_LOADING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();


    }

    public void setData(final Call<ResponseBody> getdata, final int status_call) {
        Log.e(TAG, "adapterRequest: run");

        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
                try {
                    //Log.e(TAG, "onResponse: "+response.body().string());
                    dataJson = new JSONObject(response.body().string());
                    Log.e(TAG, "onResponse: " + dataJson);
                    if (dataJson != null) {
                        String status = dataJson.getJSONObject("body_message")
                                .getString("status");
                        message_out = dataJson.getJSONObject("body_message")
                                .getString("message");
                        if (Boolean.valueOf(status)) {
                            new RefreshDataUser().create_session(dataJson.getJSONObject("body"), MainAkun.this);

                            get_data();//finish();
                        }
//                        switch (status_call) {
//                            case 1:
//                                //refersh
//                                set_new_session(dataJson);
//                                get_data();
//                                break;
//                            case 2:
//                                String status = dataJson.getJSONObject("body_message")
//                                        .getString("status");
//                                if (Boolean.valueOf(status)) {
//                                    new RefreshDataUser().create_session(dataJson.getJSONObject("body"), MainAkun.this);
//                                    finish();
//                                }
//                                //Log.e(TAG, "onResponse: "+dataJson.toString());
//                                //update
//                                break;
//                        }
                        Toast.makeText(MainAkun.this, message_out, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "onResponse: IOException: " + e.getMessage());
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: JSONException: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(MainAkun.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER, Toast.LENGTH_LONG).show();
            }
        });

    }


    private void set_new_session(JSONObject dataJson) {
//        Log.e(TAG, "set_new_session: " + dataJson);
        JSONObject dataMessage = null;
        try {
            dataMessage = dataJson.getJSONObject("body_message");
            String status_message = dataMessage.getString("status");

            if (status_message != null && Boolean.valueOf(status_message) == true) {
                JSONObject dataMain = dataJson.getJSONObject("body");
//                Log.e(TAG, "set_new_session: "+dataMain);
                new RefreshDataUser().create_session(dataMain, MainAkun.this);
            }
        } catch (JSONException e) {
            Log.e(TAG, "set_new_session: " + e.getMessage());
        }


    }
}
