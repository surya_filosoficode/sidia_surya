package com.example.lenovo.ncc_smartcity.tanggap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lenovo.ncc_smartcity.MainActivity;
import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.URLCollection;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.example.lenovo.ncc_smartcity.internalLib.RetrofitLib.request_management.SendDataTanggap;
import com.example.lenovo.ncc_smartcity.internalLib.SessionCheck;
import com.example.lenovo.ncc_smartcity.login.MainLogin;
import com.example.lenovo.ncc_smartcity.tanggap.tanggap_model.List_Post;
import com.example.lenovo.ncc_smartcity.tanggap.tanggap_model.ResponseGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.github.kobakei.materialfabspeeddial.FabSpeedDial;
import io.github.kobakei.materialfabspeeddial.FabSpeedDialMenu;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by arimahardika on 14/03/2018.
 */

public class Main_tanggap extends Activity {

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";

    RecyclerView recyclerView;
    ProgressDialog progressDialog;
    //data base
    SendDataTanggap base_url_management;
    Call<ResponseGet> getdata;
    //Status Call ==> home = 0; dashbrd = 1; search = 2;
    int statusCall = 0;
    ImageView btn_home, btn_dash, btn_takePic;
    Intent intent;
    String gak_login = "mohon maaf untuk mengakses layanan ini, anda harus login terebih dahulu";
    String id_user_x = null;
    FabSpeedDial fabSpeedDial;
    String type_ca = "1";
    private RecyclerView.Adapter mAdapter;
    private String TAG = "DashActivity";
    private List<List_Post> mItems = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.main_tanggap);

        try {
            String[] data_session = new SessionCheck().seesionLoginChecker(Main_tanggap.this);
            Log.e(TAG, "onClick: " + data_session);
            if (data_session == null) {
                intent = new Intent(Main_tanggap.this, MainLogin.class);
                startActivity(intent);
                Toast.makeText(Main_tanggap.this, gak_login, Toast.LENGTH_LONG).show();

                finish();

            } else {
                Log.e(TAG, "onClick: " + data_session[0]);

                if (Boolean.valueOf(data_session[0])) {
                    id_user_x = data_session[1];
                    //txt_id.setText(data_session[2]);
                    Log.e(TAG, "onCreate: " + data_session[3]);
                }
                grabDataTanggap();
            }
        } catch (Exception e) {
            Log.e(TAG, "onCreate: " + e);
        }

        initialize();
    }

    private void initialize() {
        recyclerView = findViewById(R.id.tanggap_recyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        btn_home = findViewById(R.id.btn_home);
        btn_dash = findViewById(R.id.btn_dash);
        btn_takePic = findViewById(R.id.btn_takePic);

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusCall = 0;
                grabDataTanggap();
            }
        });

        btn_dash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusCall = 1;

//                Toast.makeText(Main_tanggap.this, String.valueOf(statusCall), Toast.LENGTH_LONG).show();
                grabDataTanggap();
            }
        });

        btn_takePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main_tanggap.this, PostData.class);
                startActivity(intent);
            }
        });

        //initializeFAB();

//        fabSpeedDial.addOnMenuItemClickListener(new FabSpeedDial.OnMenuItemClickListener() {
//            @Override
//            public void onMenuItemClick(FloatingActionButton miniFab, @Nullable TextView label, int itemId) {
//                Log.d("TANGGAP_FAB", "Minifab " + miniFab);
//                Log.d("TANGGAP_FAB", "Label " + label);
//                Log.d("TANGGAP_FAB", "itemId " + itemId);
//            }
//        });
    }

//    private void initializeFAB() {
//        fabSpeedDial = findViewById(R.id.tanggap_fabSpeedDial);
//
//        FabSpeedDialMenu menu = new FabSpeedDialMenu(this);
//        menu.add("").setIcon(R.drawable.ic_person_white_24dp);
//        menu.add("").setIcon(R.drawable.ic_lock_white_24dp);
//        menu.add("").setIcon(R.drawable.ic_add_white_24px);
//        menu.add("").setIcon(R.drawable.ic_search_white_24px);
//        menu.add("").setIcon(R.drawable.ic_photo_camera_white_72dp);
//        menu.add("").setIcon(R.drawable.ic_account_circle_white_24px);
//        menu.add("").setIcon(R.drawable.ic_arrow_back_white_24px);
//        menu.add("").setIcon(R.drawable.ic_person_white_24dp);
//        menu.add("").setIcon(R.drawable.ic_maps_marker_black_50dp);
//        menu.add("").setIcon(R.drawable.ic_home_black_50dp);
//        fabSpeedDial.setMenu(menu);
//    }

    private void grabDataTanggap() {
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        //set base url
        base_url_management = Base_url.getClient(BASE_URL)
                .create(SendDataTanggap.class);
        RequestBody token = MultipartBody.create(MediaType.parse("multipart/form-data"), TOKEN_MOBILE);

        switch (statusCall) {
            case 0:
                //set type request uses --> internalLib->RequestManagement
                getdata = base_url_management.getItemAll(token);
                break;
            case 1:
                //add filter body
                RequestBody id_user = MultipartBody.create(MediaType.parse("multipart/form-data"), id_user_x);

                //set type request uses --> internalLib->RequestManagement
                getdata = base_url_management.getItemDash(id_user, token);
                break;
            case 2:
                //add filter body
                RequestBody id_type = MultipartBody.create(MediaType.parse("multipart/form-data"), type_ca);

                //set type request uses --> internalLib->RequestManagement
                getdata = base_url_management.getItemCa(id_type, token);
                break;
        }
        //call method request adapter
        adapterRequest(getdata);

        Log.e(TAG, "grabDataTanggap: run");

    }

    public void adapterRequest(Call<ResponseGet> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseGet>() {
            @Override
            public void onResponse(Call<ResponseGet> call, retrofit2.Response<ResponseGet> response) {
                progressDialog.dismiss();

                //JSONObject json_message = new JSONObject(response.body().getBody_message().toString());
//                try {
//                    Log.e(TAG, "onResponse: "+response.body().getBody_message().toString());
//                } catch (Exception e) {
//                    Log.e(TAG, "onResponse: "+e.toString() );
//                }
                //
                try {
                    mItems = response.body().getBody();

                    //home = 0; dashbrd = 1; search = 2;
                    Log.e(TAG, "onResponse: " + response.body().getBody().get(0).getEvent_description());

                    mAdapter = new Adapter_tanggap(mItems,
                            Main_tanggap.this, statusCall);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e);
                }
            }

            @Override
            public void onFailure(Call<ResponseGet> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Main_tanggap.this, "Request Failure Please Refresh your Apps :)", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t);
            }
        });

    }
}
