package com.example.lenovo.ncc_smartcity.info_layanan;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.lenovo.ncc_smartcity.R;
import com.example.lenovo.ncc_smartcity._globalVariable.StaticVariable;
import com.example.lenovo.ncc_smartcity.info_layanan.Utilities.Adapter_Layanan;


public class Main_Layanan_First_Level extends Activity {

    Adapter_Layanan adapterLayanan;
    RecyclerView recyclerView;
    TextView textView;
    CharSequence[] listLayanan;
    String selectedFeature;
    Typeface customFont;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layanan_first_level);

        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        textView = findViewById(R.id.tv_pariwisata_list);
        textView.setTypeface(customFont);

        recyclerView = findViewById(R.id.layanan_recycler_1st_level);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getExtra();

        getContentBasedOnCategory(selectedFeature);
    }

    private void getContentBasedOnCategory(String selected) {
        switch (selected) {
            case StaticVariable.KEPENDUDUKAN:
                textView.setText(getResources().getString(R.string.pelayananKependudukan));

                listLayanan = getResources().getTextArray(R.array.list_layanan_dispenduk);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.PAJAK:
                textView.setText(getResources().getString(R.string.pelayananPajak));

                listLayanan = getResources().getTextArray(R.array.list_layanan_pajak);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.PERIZINAN:
                textView.setText(getResources().getString(R.string.pelayananPerizinan));

                listLayanan = getResources().getTextArray(R.array.list_layanan_perizinan);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;

            case StaticVariable.KESEHATAN:
                textView.setText(getResources().getString(R.string.pelayananKesehatan));

                listLayanan = getResources().getTextArray(R.array.list_layanan_kesehatan);
                adapterLayanan = new Adapter_Layanan(listLayanan, getApplicationContext());
                recyclerView.setAdapter(adapterLayanan);
                break;
        }
    }

    private void getExtra() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            selectedFeature = (String) bundle.get(StaticVariable.NAMA_LAYANAN);
        }
    }


}
